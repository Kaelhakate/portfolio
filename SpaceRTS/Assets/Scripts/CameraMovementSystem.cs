﻿using Unity.Entities;
using UnityEngine;

namespace SpaceRTS.Camera
{
    public class CameraMovementSystem : ComponentSystem // Again as we are using main thread
    {
        [Inject] private CameraData camData; // The entity "filter"
        private const float moveSpeed = 100f;
        private const float zoomSpeed = 75f;
        private const float lookSpeed = 25f;
        protected override void OnUpdate()
        {
            for (int ii = 0; ii < camData.Length; ii++)
            {
                if(camData.cameraComponent[ii].tag.Equals("MainCamera"))// I know could just access the main via the reference but this is an exercise of ECS
                {
                    moveCam(camData.cameraMovements[ii], camData.cameraTransform[ii]);
                    lookCam(camData.cameraMovements[ii], camData.cameraTransform[ii]);
                }
            }
        }
        /// <summary>
        /// Applies the movement to the transform. To be used with cameras.
        /// </summary>
        /// <param name="m">The movement in question.</param>
        /// <param name="t">The transform to be affected.</param>
        private void moveCam(CameraMovement m, Transform t)
        {
            Vector3 v;
            if((Mathf.Abs(m.truck) + Mathf.Abs(m.pedestal) + Mathf.Abs(m.dolly) - float.Epsilon) > 0.0f)
            {
                v = new Vector3(m.truck, m.pedestal, m.dolly);
                v *= moveSpeed * Time.deltaTime; // Scale to the speed of movement in time
                t.Translate(v, Space.Self);// Apply the movement
            }
        }
        /// <summary>
        /// Applies the movement regarding looking to the transform. To be used with cameras.
        /// </summary>
        /// <param name="m">The movement in question.</param>
        /// <param name="t">The transform to be affected.</param>
        private void lookCam(CameraMovement m, Transform t)
        {
            Quaternion q;
            if(m.doRoate != 0)
            {
                if (Mathf.Abs(m.pan) - float.Epsilon > 0.0f)
                {
                    q = Quaternion.AngleAxis(m.pan * lookSpeed * Time.deltaTime, Vector3.up); // Rotate along x thus around y
                    t.rotation *= q; // Apply
                }
                if (Mathf.Abs(m.tilt) - float.Epsilon > 0.0f)
                {
                    q = Quaternion.AngleAxis(m.tilt * lookSpeed * Time.deltaTime, Vector3.left); // The opposite of pan
                    t.rotation *= q;
                }
            }
            if (Mathf.Abs(m.zoom) - float.Epsilon > 0.0f)
            {
                t.Translate(Vector3.forward * m.zoom * zoomSpeed * Time.deltaTime, Space.Self); // Zoom in/out in direction facing
            }
        }
    }
}
