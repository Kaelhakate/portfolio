﻿using Unity.Entities;
using Unity.Transforms;
using Unity.Rendering;

namespace SpaceRTS
{
    // For code reuse
    public struct BaseUnit : IComponentData
    {
        public Position pos;
        public Rotation rot;
        public Scale scale;
        public MeshInstanceRenderer mesh;
        public Health hp;
    }
}
