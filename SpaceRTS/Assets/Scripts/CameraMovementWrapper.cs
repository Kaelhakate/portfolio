﻿using System;
using Unity.Entities;

namespace SpaceRTS.Camera
{
    public class CameraMovementWrapper: ComponentDataWrapper<CameraMovement>
    {
        // So we can use it for the hybrid cameras.
    }
}
/*
public class CameraMovement : MonoBehaviour {

    public float panSpeed = 50f;
    public float scrollSpeed = 500f;
    public float rotateSpeed = 50f;
	// Update is called once per frame
	void Update ()
    {
        float panRate = panSpeed * Time.deltaTime; // Save on repetition
        // Inputs
        float strafe = Input.GetAxis("CamStrafe"), alt = Input.GetAxis("CamAlt"), fore = Input.GetAxis("CamForeBack"), 
              scroll = Input.GetAxis("Mouse ScrollWheel"), mouseX = Input.GetAxis("Mouse X"), mouseY = Input.GetAxis("Mouse Y");
        if(alt != 0)
        {
            gameObject.transform.position += new Vector3(0, alt, 0) * Time.deltaTime * panSpeed;
        }
        if(strafe != 0 || fore != 0)
        {
            gameObject.transform.Translate(strafe * panRate, 0, fore * panRate, Space.Self);
            Debug.Log("Moved");
        }
        if (scroll != 0)
        {
            gameObject.transform.position += (gameObject.transform.rotation) * Vector3.forward * scroll * Time.deltaTime * scrollSpeed;
        }
        if(Input.GetButton("RotateCam"))
        {
            Quaternion rotation = gameObject.transform.rotation;
            if(mouseX != 0)
            {
                rotation *= Quaternion.AngleAxis(1 * rotateSpeed * Time.deltaTime * mouseX, Vector3.up);
            }
            if(mouseY != 0)
            {
                rotation *= Quaternion.AngleAxis(1 * rotateSpeed * Time.deltaTime * mouseY, Vector3.left);
            }
            gameObject.transform.rotation = rotation;
        }
	}
}*/
