﻿using UnityEngine;
using Unity.Entities;
using Unity.Transforms;
using Unity.Rendering;
/// <summary>
/// Provides the archetypes for the entire game
/// </summary>
namespace SpaceRTS
{
    public class Units
    {
        public static EntityArchetype baseUnit;
        public static bool generateUnits(EntityManager manager)
        {
            bool result = true;
            ComponentType[] baseTypes = { typeof(Position), typeof(Rotation), typeof(Scale), typeof(Health) };

            baseUnit = manager.CreateArchetype(baseTypes);
            return result;
        }
    }
}
