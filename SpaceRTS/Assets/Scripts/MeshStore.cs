﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unity.Entities;
using Unity.Rendering;
using UnityEngine;

namespace SpaceRTS
{
    class MeshStore
    {
        private static Dictionary<EntityArchetype, MeshInstanceRenderer> storage = new Dictionary<EntityArchetype, MeshInstanceRenderer>();

        /// <summary>
        /// Finds a prototype in the scene, extracts the mesh and removes the prototype.
        /// </summary>
        /// <param name="target">The name of the prototype.</param>
        /// <param name="key">The key to register the mesh against.</param>
        /// <returns>Confirmation if successful.</returns>
        public bool extractMesh(String target, EntityArchetype key)
        {
            bool result = true;
            GameObject proto = GameObject.Find(target);
            // TODO: Find a better way to load meshes, than gameobject prototypes (Hyrbid in my books, I want pure)
            return result;
        }
    }
}
