﻿using Unity.Entities;
using UnityEngine;
/// <summary>
/// Handles placing the user inputs into camera's
/// </summary>
namespace SpaceRTS
{
    namespace Camera
    {
        public class CameraInputSystem : ComponentSystem // Seeing as we need the main thread API
        {
            [Inject] private CameraData camData; // Inject tells unity to create a struct that follows the definition/filter.

            protected override void OnUpdate()
            {
                // Inputs
                float truck = Input.GetAxis("CamStrafe"), pedestal = Input.GetAxis("CamAlt"), dolly = Input.GetAxis("CamForeBack"),
                      zoom = Input.GetAxis("Mouse ScrollWheel"), pan = Input.GetAxis("Mouse X"), tilt = Input.GetAxis("Mouse Y");
                byte doRotate = (byte)(Input.GetButton("RotateCam") ? 1 : 0); // Have to cast as doesn't do implicit here probably due to unity + potential negative loss
                for (int ii = 0; ii < camData.Length; ii++)
                {
                    camData.cameraMovements[ii] = new CameraMovement( truck, pedestal, dolly, zoom, pan, tilt, doRotate);
                }
            }
        }
    }
}
