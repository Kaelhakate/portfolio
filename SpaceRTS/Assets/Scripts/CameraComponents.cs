﻿using System;
using Unity.Entities;
using UnityEngine;
/// <summary>
/// Used to save code repetition for structs
/// </summary>
namespace SpaceRTS.Camera
{
    public struct CameraData // Acts as a filter for the camera entities
    {
        public readonly int Length; // Gets populated by c#/unity
        public ComponentDataArray<CameraMovement> cameraMovements; // The filter its self.
        public ComponentArray<Transform> cameraTransform; // Filter for transforms too, TODO: Once ECS is complete change out of hybrid.
        public ComponentArray<UnityEngine.Camera> cameraComponent; // Most importantly
    }
    [SerializableAttribute()]
    public struct CameraMovement : IComponentData
    {   // Using proper camera terms
        public float dolly; //In out
        public float truck; // left right
        public float pedestal; // up down
        public float pan;
        public float tilt;
        public float zoom;
        public byte doRoate; // As bool is not blit-able (requirement of structs in ECS)

        public CameraMovement(float truck, float pedestal, float dolly, float zoom, float pan, float tilt, byte doRotate)
        {
            this.truck = truck;
            this.pedestal = pedestal;
            this.dolly = dolly;
            this.zoom = zoom;
            this.pan = pan;
            this.tilt = tilt;
            this.doRoate = doRotate;
        }
    }
}
