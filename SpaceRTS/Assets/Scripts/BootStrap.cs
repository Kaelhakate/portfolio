﻿using UnityEngine;
using Unity.Entities;

namespace SpaceRTS
{
    /// <summary>
    /// Sets off the logic in the scene.
    /// </summary>
    public class BootStrap
    {
        /// <summary>
        /// Fires before the scene is shown, e.g used to create the archetypes.
        /// </summary>
        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
        public static void initialize()
        {
            EntityManager manager = World.Active.GetOrCreateManager<EntityManager>();
            Units.generateUnits(manager);
        }
        /// <summary>
        /// Controls the logic to occur after the scene has loaded in.
        /// </summary>
        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.AfterSceneLoad)]
        public static void initializeAfter()
        {
            EntityManager manager = World.Active.GetOrCreateManager<EntityManager>();
            manager.CreateEntity(Units.baseUnit);
            Debug.Log("Loaded");
        }
    }
}
