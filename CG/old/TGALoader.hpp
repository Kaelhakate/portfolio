/*
    TGALoader
    Seeing as the example wasn't too clear, it was using glaux and some windows stuff,
    aswell as using BMP files, I was reading that TGA's we even more straight forward so 
    I found this and Adpated for my own uses: 
    https://stackoverflow.com/questions/7046270/loading-a-tga-file-and-using-it-with-opengl
    and confirmed fields with this
    http://www.paulbourke.net/dataformats/tga/
*/
#include <stdio.h>
#include <iostream>

#ifndef HAS_GL
#include <GL/gl.h>
#include <GL/glut.h>
#define HAS_GL
#endif

struct Tga_Payload{
    unsigned char* data;
    int width;
    int height;
    int colorMode;
};

class TGALoader
{
    private:
        static Tga_Payload load(char const* target);
    public:
        static GLuint loadTexture(char const* target);
};