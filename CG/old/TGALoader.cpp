#include "TGALoader.hpp"


GLuint TGALoader::loadTexture(char const* target)
{
    Tga_Payload result = load(target);
    GLuint tex;
    GLenum iFormat = GL_RGB;
    
    glGenTextures(1, &tex);
    glBindTexture(GL_TEXTURE_2D, tex);
    
    if(result.colorMode = 4)
    {
        iFormat = GL_RGBA;
    }
    
    glTexImage2D(GL_TEXTURE_2D, 0, iFormat, result.width, result.height, 0, iFormat, GL_UNSIGNED_BYTE, result.data);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
    
    delete result.data;
    return tex;
}

Tga_Payload TGALoader::load(char const* target)
{
    FILE *fPtr;
    Tga_Payload out;
    unsigned char *res = NULL;
    int colorMode;
    short int sIntTemp, width, height;
    unsigned char uCharTemp, imageType, bitCount,temp;
    long imageSize;
    
    
    fPtr = fopen(target, "rb");
    if(fPtr != NULL)
    {
        // Skip the first part of the header
        fread(&uCharTemp, sizeof(unsigned char), 1, fPtr); // The id field
        fread(&uCharTemp, sizeof(unsigned char), 1, fPtr); // Colour map type
        fread(&imageType, sizeof(unsigned char), 1, fPtr);  // Read in the image type
        
        if(imageType == 2 || imageType == 3) // Only accept RGB or BW uncompressed(both)
        {
             // Next stuff to skip - Colour map  and part of the Image specification
            fread(&sIntTemp, sizeof(short int), 1, fPtr);    
            fread(&sIntTemp, sizeof(short int), 1, fPtr);
            fread(&uCharTemp, sizeof(unsigned char), 1, fPtr);
            fread(&sIntTemp, sizeof(short int), 1, fPtr);
            fread(&sIntTemp, sizeof(short int), 1, fPtr);
            
            fread(&width, sizeof(short int), 1, fPtr); //Width
            fread(&height, sizeof(short int), 1, fPtr); // Height
            fread(&bitCount, sizeof(unsigned char), 1, fPtr);// BitCount
            //One more skip
            fread(&uCharTemp, sizeof(unsigned char), 1, fPtr); // Image descriptor byte
            // Now the proper loading
            colorMode = bitCount / 8; // Difference between BGR and BGRA
            imageSize = width * height * colorMode; // Pixel count
            
            res =  new unsigned char[imageSize]; // Allocating the image space in memory
            fread(res, sizeof(unsigned char), imageSize, fPtr);
            
            for(int ii = 0; ii < imageSize; ii += colorMode) 
            // Switching in RGB arrangement Apparently Opengl does handle BGRA however I 
            // left this in for universalness if I ever use other formats
            {
                temp = res[ii];
                res[ii] = res[ii+2];
                res[ii + 2] = temp; 
            }
            out.width = width;
            out.height = height;
            out.colorMode = colorMode;
        }
        fclose(fPtr);
    }
    else
    {
        std::cerr << "Unable to open: " << target << "\n";
    }
    out.data = res;
    return out;
}