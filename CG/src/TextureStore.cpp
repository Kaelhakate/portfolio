#include "headers/TextureStore.hpp"
#define STB_IMAGE_IMPLEMENTATION
#include "headers/stb_image.h"
#include <iostream>

TextureStore* TextureStore::instance = NULL;

TextureStore::TextureStore()
{
    if(instance != NULL)
    {
        throw std::bad_alloc();// An instance already exists
    }
    instance = this;
}
TextureStore::~TextureStore()
{
    std::map<int, GLuint>::iterator itr;
    for(itr = textures.begin(); itr != textures.end(); itr++)
    {
        glDeleteTextures(1, &(itr->second));
    }
    textures.clear();
}
/*
    hashTarget
    Seeing as valgrind was throughing some errors relating to the use a string key i figured just using ints would be better
*/
int TextureStore::hashTarget(char const* target)
{
    int hash = 0;
    std::string text(target);
    std::string::iterator iter;
    
    for(iter = text.begin(); iter != text.end(); iter++)
    {
        hash += (*iter) * 17;
    }
    hash /= 3;
    return hash;
}


TextureStore* TextureStore::getInstance()
{
    if(instance == NULL)
    {
        new TextureStore();
    }
    return instance;
}

GLuint TextureStore::getTexture(char const* target)
{
    GLuint res = 0;
    std::map<int, GLuint>::iterator itr;
    int hash = hashTarget(target);
    itr = textures.find(hash);
    if(itr != textures.end())
    {
        res = itr->second;
    }
    else
    {
        res = loadTexture(target);
        if(res != 0)
        {
            textures[hash] = res;
        }
    }
    return res;
}
/*
    loadTexture
    Loads a desired file and returns its texture handle in memory
*/
GLuint TextureStore::loadTexture(char const* target)
{
    int width;
    int height;
    int channels;
    
    GLuint tex = 0;
    stbi_set_flip_vertically_on_load(1); // Get stbi in line with opengl coords
    
    std::cout << "Loading Texture: " << target;
    unsigned char* imgData = stbi_load(target, &width, &height, &channels, 4); // Always rgba
        
    if(imgData != NULL)
    {
        
        glGenTextures(1, &tex); // Here we generate the ID for the texture
        glBindTexture(GL_TEXTURE_2D, tex); // // Bind the incoming data to the ID
               
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST); // Use linear blend of mipmaps
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR); // When texture is being upscaled use linear filtering
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, imgData);
        
        stbi_image_free(imgData);
        std::cout << " Success!" << "\n";
    }
    else
    {
        std::cout << " Failed!" << "\n";
    }
    return tex;
}