#include "headers/Scene.hpp"

#define MAX_DISTANCE 195
/* CONSTRUCTOR */
Scene::Scene(Camera* inCam)
{
    sky = new Skybox(inCam, this);
    parent=NULL;
    SceneObject *o;
    TerrainTile* t;
    for(int xi = -25; xi < 26; xi += 5)
    {
        for(int zi = -MAX_DISTANCE; zi < 6; zi += 5)
        {
            if(xi != 0)
            {
                t = new GrassTile((float)xi, 0, (float)zi, this);
            }
            else
            {
                t = new RoadTile((float)xi, 0, (float)zi, this, (zi%3==0));// We want a pylon every 15m
            }
            
            terrainList.push_back(t);
        }
    }
}
/* DECONSTRUCTOR */
Scene::~Scene()
{
    std::list<SceneObject*>::iterator iter;
    std::list<TerrainTile*>::iterator iterT;
    
    for(iterT = terrainList.begin(); iterT != terrainList.end(); iterT++)
    {
        delete (*iterT);
    }
    terrainList.clear();
    
    for(iter = objList.begin(); iter != objList.end(); iter++)
    {
        delete *iter;
    }
    objList.clear();
    
    delete sky;
}
/*
 * draw
 * IMPORT:
 *  float - delta - The time since the last frame
 */
void Scene::draw(float speed, float delta)
{
    static float secondAcc = 0;
    secondAcc += delta;
    
    
    bool doDistCheck = secondAcc > 1; // We want to do distance checks once a second to save on sqrt cycles
    if(doDistCheck)
    {
        secondAcc = 0;
    }
    Camera* cam = Camera::getMainCamera();
    Vector3* pos;
    std::list<SceneObject*>::iterator iterO;
    std::list<TerrainTile*>::iterator iterT;
    sky->render(speed, delta);
    // Now the terrain
    updateTerrain(speed, delta);
    for(iterT = terrainList.begin(); iterT != terrainList.end(); iterT++)
    {
        if(doDistCheck)
        {
            pos = (*iterT)->getGlobalPosition();
            (*iterT)->setIsInSight(cam->checkIfInSight(pos));
            delete pos;
        }
        if((*iterT)->isInSight())
        {
            (*iterT)->render(speed, delta);
        }
    }
    
    for(iterO = objList.begin(); iterO != objList.end(); iterO++)
    {
        if(doDistCheck)
        {
            pos = (*iterO)->getGlobalPosition();
            (*iterO)->setIsInSight(cam->checkIfInSight(pos));
            delete pos;
        }
        if((*iterO)->isInSight())
        {
            (*iterO)->render(speed, delta);
        }
    }
}
/*
    updateTerrain
    Maintains the scrolling of the terrain
    speed - The rate the scrolling goes
    delta - Time since the last call (Inteded for each frame) allowing for smooth animation
*/

void Scene::updateTerrain(float speed, float delta)
{
    const float X_RATE = 0.5f;
    const float Y_RATE = 0.5f;
    const float Z_RATE = 8.0f; // 8m/s
    
    const int SCROLL_ON_X = 0;
    const int SCROLL_ON_Y = 0;
    const int SCROLL_ON_Z = 1;
    
    static int flipSwitch = 0;
    
    std::list<TerrainTile*>::iterator iter;
    TerrainTile* temp;
    float x = SCROLL_ON_X*X_RATE*speed*delta;
    float y = SCROLL_ON_Y*Y_RATE*speed*delta;
    float z = SCROLL_ON_Z*Z_RATE*speed*delta;
    int flipStatus = 1;
    if(flipSwitch == 2)
    { // Here every three replacements we flip the positions of left and right of the road to change it up a bit
        flipStatus = -1;
        flipSwitch = 0;
    }
        
    for(iter = terrainList.begin(); iter != terrainList.end(); iter++)
    {
        temp = (*iter);
        temp->movePosition(x,y,z);
        if(temp->getPosition()->z > 5) // We want to wrap around the scene
        { // We also flip the positioning around the road(0) just to and some difference
            temp->setPosition(flipStatus * temp->getPosition()->x,temp->getPosition()->y,temp->getPosition()->z - MAX_DISTANCE);
        }
    }
    flipSwitch++;
}
