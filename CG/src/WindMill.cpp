#include "headers/WindMill.hpp"
#include "headers/WindMillTop.hpp"

const Mesh* WindMill::mesh_lod_1 = NULL;
const Mesh* WindMill::mesh_lod_2 = NULL;
GLuint WindMill::wmillTexture = 0;
const GLfloat WindMill::wmillAmbient[4] = {0.125, 0.125, 0.125, 1.0};
const GLfloat WindMill::wmillDiffuse[4] = {0.75, 0.75, 0.75, 1.0};
const GLfloat WindMill::wmillSpecular[4] = {1, 1, 1, 1.0};
const GLfloat WindMill::wmillShininess = 100.0f;

/* CONSTRUCTOR */
WindMill::WindMill()
{
    if(mesh_lod_1 == NULL)
    {
        mesh_lod_1 = MeshStore::getInstance()->getMesh("models/wmill_base_1.obj");
    }
    if(mesh_lod_2 == NULL)
    {
        mesh_lod_2 = MeshStore::getInstance()->getMesh("models/wmill_base_2.obj");
    }
    topSection = new WindMillTop(0, 6.0f, 0, this);
    topSection->rotateY(rand() % 360); // give a random orientation
}
/* ALTERNATE CONSTRUCTOR */
WindMill::WindMill(float inx, float iny, float inz, SceneObject* parent): SceneObject(inx, iny, inz, parent)
{
    if(mesh_lod_1 == NULL)
    {
        mesh_lod_1 = MeshStore::getInstance()->getMesh("models/wmill_base_1.obj");
    }
    if(mesh_lod_2 == NULL)
    {
        mesh_lod_2 = MeshStore::getInstance()->getMesh("models/wmill_base_2.obj");
    }
    if(wmillTexture == 0)
    {
         wmillTexture = TextureStore::getInstance()->getTexture("textures/wmill.jpg");
    }
    topSection = new WindMillTop(0, 6.0f, 0, this);
    topSection->rotateY(rand() % 360); // give a random orientation
}
/* DECONSTRUCTOR */
WindMill::~WindMill()
{
    delete topSection;
}
/*
    draw
    Draws the meshes of the windmill.
*/
void WindMill::draw(float speed, float delta)
{    
    glMaterialfv(GL_FRONT, GL_AMBIENT, wmillAmbient);
    glMaterialfv(GL_FRONT, GL_DIFFUSE, wmillDiffuse);
    glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, wmillSpecular);
    glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, wmillShininess);
    
    glBindTexture(GL_TEXTURE_2D, wmillTexture);
    glEnable(GL_TEXTURE_2D);
    if(lod == LOD_2)
    {
        mesh_lod_2->draw();
    }
    else
    {
        mesh_lod_1->draw();
    }
    topSection->render(speed, delta);
    glDisable(GL_TEXTURE_2D);
}