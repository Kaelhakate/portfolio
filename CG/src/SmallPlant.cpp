#include "headers/SmallPlant.hpp"

bool SmallPlant::HAS_TEXT = false;
GLuint SmallPlant::textures[9] = {0};

/* We want a greeny/brown tinge to the texture to make it appear as more of a shrub */
const GLfloat SmallPlant::plantAmbient[4] = {0.15, 0.2, 0.15, 1.0};
const GLfloat SmallPlant::plantDiffuse[4] = {0.4, 0.43, 0.42, 1.0};
const GLfloat SmallPlant::plantSpecular[4] = {0.5, 0.55, 0.5, 1.0};
const GLfloat SmallPlant::plantShininess = 0.125f;

/* DEFAULT CONSTRUCTOR */
SmallPlant::SmallPlant()
{
    if(!HAS_TEXT)
    {
        loadTextures();
        HAS_TEXT = true;
    }
    parent = NULL;
    selectedText = rand() % 10;
}
/* ALTERNATE CONSTRUCTOR - Places the plant at the location */
SmallPlant::SmallPlant(float inx, float iny, float inz, SceneObject* parent):SceneObject(inx, iny, inz, parent)
{
    if(!HAS_TEXT)
    {
        loadTextures();
        HAS_TEXT = true;
    }
    selectedText = rand() % 9;
}
/* DECONSTRUCTOR */
SmallPlant::~SmallPlant(){}
/*
 * loadTextures
 * Loads the textures to be used by the plants variations
 */
void SmallPlant::loadTextures()
{
    char path[24] = "textures/plants/p_0.png";
    for(int ii = 0; ii < 9; ii++)
    {
        path[18] = (ii + '0');
        textures[ii] = TextureStore::getInstance()->getTexture(path);
    }
}
/*
 * draw
 * The inherited draw method that displays the plant
 * IMPORT:
 *     float - speed - Rate animations should run at 
 *     float - delta - The time since the last frame
 */
void SmallPlant::draw(float speed, float delta)
{   
    glMaterialfv(GL_FRONT, GL_AMBIENT, plantAmbient);
    glMaterialfv(GL_FRONT, GL_DIFFUSE, plantDiffuse);
    glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, plantSpecular);
    glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, plantShininess);  
  
    if(lod == LOD_0)
    {
        // Do not draw
    }
    else if(lod == LOD_1)
    { // Draw a solid color quad
        glColor3f(0.6f, 0.4f, 0.2f); // brownish colour
        glBegin(GL_QUADS); // AVG RATIO FOR TEXTURES IS ~1:1.5
            glNormal3f(0, 0, 1); glVertex3f(-0.25f, 0, 0); // Bottom Left
            glNormal3f(0, 0, 1); glVertex3f(0.25f, 0, 0); // Bottom Right
            glNormal3f(0, 0, 1); glVertex3f(0.25f, 0.75f, 0);
            glNormal3f(0, 0, 1); glVertex3f(-0.25f, 0.75f, 0);
        glEnd();
        glColor3f(1,1,1);
    }
    else
    { // Draw the proper textured plant
        glEnable (GL_ALPHA_TEST);
        glAlphaFunc(GL_GREATER, 0);
        glEnable(GL_TEXTURE_2D);
        glColor4f(1,1,1,0); //Set clear so there are no weird lines that I was getting
        glBindTexture(GL_TEXTURE_2D, textures[selectedText]);
        glBegin(GL_QUADS); // AVG RATIO FOR TEXTURES IS ~1:1.5
            glNormal3f(0, 0, 1);glTexCoord2f(0,0); glVertex3f(-0.25f, 0, 0); // Bottom Left
            glNormal3f(0, 0, 1);glTexCoord2f(1,0); glVertex3f(0.25f, 0, 0); // Bottom Right
            glNormal3f(0, 0, 1);glTexCoord2f(1,1); glVertex3f(0.25f, 0.75f, 0);
            glNormal3f(0, 0, 1);glTexCoord2f(0,1); glVertex3f(-0.25f, 0.75f, 0);
         glEnd();
         glDisable(GL_TEXTURE_2D);
         
         glDisable(GL_ALPHA);
    }
    /* Revoke Material Bindings */
    glMaterialfv(GL_FRONT, GL_AMBIENT, defaultAmb);
    glMaterialfv(GL_FRONT, GL_DIFFUSE, defaultDiff);
    glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, defaultSpec);
    glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, defaultShine);  
}