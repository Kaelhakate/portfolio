#include "headers/Shed.hpp"

GLuint Shed::shedTexture= 0;
const Mesh* Shed::lod_0_mesh = NULL;
const Mesh* Shed::lod_1_mesh = NULL;
const Mesh* Shed::lod_2_mesh = NULL;
/* We want a reddish tinge to accentuate the rust spots on the texture */
const GLfloat Shed::shedAmbient[4] = {0.2, 0.125, 0.125, 1.0};
const GLfloat Shed::shedDiffuse[4] = {0.2, 0.2, 0.2, 1.0};
const GLfloat Shed::shedSpecular[4] = {0.3, 0.25, 0.15, 1.0};
const GLfloat Shed::shedShininess = 0.2f;



Shed::Shed(float x, float y, float z, SceneObject* parent): SceneObject(x,y,z, parent)
{
    if(lod_0_mesh == NULL)
    {
        lod_0_mesh = MeshStore::getInstance()->getMesh("models/shed_0.obj");
    }
    if(lod_1_mesh == NULL)
    {
        lod_1_mesh = MeshStore::getInstance()->getMesh("models/shed_1.obj");
    }    
    if(lod_2_mesh == NULL)
    {
        lod_2_mesh = MeshStore::getInstance()->getMesh("models/shed_2.obj");
    }
    if(shedTexture == 0)
    {
         shedTexture = TextureStore::getInstance()->getTexture("textures/shed.jpg");
    }
    rotateY(rand() % 360); // Give a random starting rotation
}
Shed::~Shed(){}

void Shed::draw(float speed, float delta)
{
    glMaterialfv(GL_FRONT, GL_AMBIENT, shedAmbient);
    glMaterialfv(GL_FRONT, GL_DIFFUSE, shedDiffuse);
    glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, shedSpecular);
    glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, shedShininess);
    
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, shedTexture);
    if(lod == LOD_0)
    {
        lod_0_mesh->draw();
    }
    else if(lod == LOD_1)
    {
        lod_1_mesh->draw();
    }
    else
    {
        lod_2_mesh->draw();
    }
    glDisable(GL_TEXTURE_2D);
    /* Revoke Material Bindings */
    glMaterialfv(GL_FRONT, GL_AMBIENT, defaultAmb);
    glMaterialfv(GL_FRONT, GL_DIFFUSE, defaultDiff);
    glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, defaultSpec);
    glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, defaultShine);  
}