#include "headers/MeshStore.hpp"

MeshStore* MeshStore::instance = NULL;
/* CONSTRUCTOR */
MeshStore::MeshStore()
{
    if(instance != NULL)
    {
        throw std::bad_alloc();// An instance already exists
    }
    instance = this;
}
/* DECONTRUCTOR */
MeshStore::~MeshStore()
{
    std::map<int, Mesh*>::iterator itr;
    for(itr = meshes.begin(); itr != meshes.end(); itr++)
    {
        delete itr->second;
    }
    meshes.clear();
}
/*
    hashTarget
    Hashes the given string, intended for use with file paths
    IMPORT:
        char const* target A string literal to be turned into a hash
    EXPORT:
        The resulting hash
*/
int MeshStore::hashTarget(char const* target)
{
    int hash = 0;
    std::string text(target);
    std::string::iterator iter;
    
    for(iter = text.begin(); iter != text.end(); iter++)
    {
        hash += (*iter) * 17;
    }
    hash /= 3;
    return hash;  
}
/*
    getInstance
    Returns the current instance of the MeshStore, creates one if there isnt
    EXPORTS:
        A pointer to the current instance of MeshStore
*/
MeshStore* MeshStore::getInstance()
{
    if(instance == NULL)
    {
        new MeshStore();
    }
    return instance;
}
/*
    getMesh
    Gets the mesh from storage, if it isnt present then load it in
    IMPORT:
        char const* A string literal that defines the path to the mesh file
*/
const Mesh* MeshStore::getMesh(char const* target)
{
    Mesh* temp;
    const Mesh* res;
    std::map<int, Mesh*>::iterator itr;
    int hash = hashTarget(target);
    itr = meshes.find(hash);
    if(itr != meshes.end())
    {
        res = itr->second;
    }
    else
    {
        temp = new Mesh(target); // Make the mesh
        meshes[hash] = temp; // Store it
        res = temp; // Now make it a const
    }
    return res;
}