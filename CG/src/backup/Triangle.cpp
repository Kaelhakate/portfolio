#include "headers/Triangle.hpp"
Triangle::Triangle()
{
    // SceneObject(); Is automatically called
}
Triangle::Triangle(float inx, float iny, float inz) : SceneObject(inx, iny, inz)
{
}
Triangle::~Triangle()
{
    //
}


void Triangle::draw(float delta)
{
    glColor3f(0.0f, 0.0f, 1.0f);
    glBegin(GL_TRIANGLES);
        glVertex3f(0.0f, 1.0f, 0.0f);
        glVertex3f(-1.0f, -1.0f, 0.0f);
        glVertex3f(1.0f, -1.0f, 0.0f);
     glEnd();
}