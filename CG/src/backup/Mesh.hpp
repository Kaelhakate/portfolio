/*
    Mesh
    My Attempt at VBO's
    Informed by http://www.morethantechnical.com/2013/11/09/vertex-array-objects-with-shaders-on-opengl-2-1-glsl-1-2-wcode/
*/
#ifndef HAS_GL
#define GL_GLEXT_PROTOTYPES
#include <GL/gl.h>
#include <GL/glut.h>
#include <GL/glx.h>
#include <GL/glext.h>

// These are defined as attributes i put into the shader loader
#define VAO_VERT 0
#define VAO_NORMAL 1
#define VAO_UV 2

#define HAS_GL
#endif
class Vertex
{
    public:
        float x;
        float y;
        float z;
};
typedef Vertex Normal;
class TextCoord
{
    public:
        float u;
        float v;
};

class Mesh
{
    private:
        int vertCount;
        Vertex* verts;
        TextCoord* textCoords;
        Normal* normals;
        GLuint vaid;
        
        void BuildVBO();
    public:

        
        unsigned int textId;
        unsigned int vBufferId;
        unsigned int nBufferId;
        unsigned int tBufferId;
        
        Mesh();
        Mesh(int count, Vertex* v, TextCoord* t, Normal* n);
        ~Mesh();
        
        int getVertCount();
        GLuint getVAID();
        // OBJ LOADER HERE;
};
