#include "headers/Mesh.hpp"
Mesh::Mesh()
{
}

Mesh::Mesh(int count, Vertex* v, TextCoord* t, Normal* n)
{
    vertCount = count;
    verts = v;
    textCoords = t;
    normals = n;
    BuildVBO();
}

Mesh::~Mesh()
{
    glDeleteBuffers(1, &vBufferId);
    glDeleteBuffers(1, &tBufferId);
}

void Mesh::BuildVBO()
{
       
    glGenBuffers(1, &vBufferId);
    glBindBuffer(GL_ARRAY_BUFFER, vBufferId);
    glBufferData(GL_ARRAY_BUFFER, vertCount * sizeof(Vertex), verts, GL_STATIC_DRAW);
    //^ First creates a handle then uploads the data to the GPU, and marks it static implying it wont be editable
    glGenBuffers(1, &nBufferId);
    glBindBuffer(GL_ARRAY_BUFFER, nBufferId);
    glBufferData(GL_ARRAY_BUFFER, vertCount * sizeof(Normal), normals, GL_STATIC_DRAW);    
     //^ Normals
    glGenBuffers(1, &tBufferId);
    glBindBuffer(GL_ARRAY_BUFFER, tBufferId);
    glBufferData(GL_ARRAY_BUFFER, vertCount * sizeof(TextCoord), textCoords, GL_STATIC_DRAW);
    // ^ TextCoords    
    glGenVertexArrays(1, &vaid); // Create the VAO
    glBindVertexArray(vaid);
    // Link the verterx vbo to the vaid
    glEnableVertexAttribArray(VAO_VERT);
    glVertexAttribPointer(VAO_VERT, 3, GL_FLOAT, GL_FALSE, 0, 0 ); // Format of the data 
    // Link the normal vbo to the vaid
    glEnableVertexAttribArray(VAO_NORMAL);
    glVertexAttribPointer(VAO_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, 0 ); // Format of the data   
    // Link the uv coords vbo to the vaid
    glEnableVertexAttribArray(VAO_UV);
    glVertexAttribPointer(VAO_UV, 2, GL_FLOAT, GL_FALSE, 0, 0 ); /// Format of the data 
            
    delete[] verts;
    delete[] textCoords;
}

int Mesh::getVertCount()
{
    return vertCount;
}

GLuint Mesh::getVAID()
{
    return vaid;
}