#ifndef HAS_SHADER
#define HAS_SHADER
#ifndef HAS_GL
#define GL_GLEXT_PROTOTYPES
#include <GL/gl.h>
#include <GL/glut.h>
#include <GL/glx.h>
#include <GL/glext.h>

#define VAO_VERT 0
#define VAO_RGB 1
#define VAO_UV 2

#define HAS_GL
#endif

/*
    ShaderStore
    Adapted from http://www.opengl-tutorial.org/beginners-tutorials/tutorial-2-the-first-triangle/
*/
#include <vector>
#include <map>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <ios>

class ShaderStore
{
    private:
        static ShaderStore* instance;
        std::map<int, GLuint> shaders;
        int hashTarget(const char* vertexShad, const char* fragShad);
        GLuint loadShaders(const char * vertex_file_path,const char * fragment_file_path);
    public:
        ShaderStore();
        ~ShaderStore();
        GLuint getShaders(const char* vertex_path, const char* frag_path);
        static ShaderStore* getInstance();     
};

#endif