#include "SceneObject.hpp"

class Triangle: public SceneObject
{
    private:
        void draw(float delta);
    public:
        Triangle();
        Triangle(float inx, float iny, float inz);
        ~Triangle();
};