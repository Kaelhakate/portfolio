#include "headers/RoadTile.hpp"

GLuint RoadTile::roadTexture = 0;
// Material values taken from black rubber here
//http://www.it.hiof.no/~borres/j3d/explain/light/p-materials.html
GLfloat RoadTile::roadAmbient[4] = {0.2f, 0.2f, 0.2f, 1.0f};
GLfloat RoadTile::roadDiffuse[4] = {0.75f, 0.75f, 0.75f,1.0f };
GLfloat RoadTile::roadSpecular[4] = {0.9f, 0.9f, 0.9f,1.0f };
GLfloat RoadTile::roadShine = 32.0f;
/* DEFAULT CONSTRUCTOR */
RoadTile::RoadTile()
{
    if(roadTexture == 0)
    {
        roadTexture = TextureStore::getInstance()->getTexture(ROAD_TEXT_PATH);
    }
    contents.push_back(new Pylon(3.0f, 0, 0, this)); // Place a pylon on the right
}
/* ALTERNATE CONSTRUCTOR */
RoadTile::RoadTile(float inx, float iny, float inz, SceneObject* parent, bool isPylon):TerrainTile(inx, iny, inz,parent)
{
    if(roadTexture == 0)
    {
        roadTexture = TextureStore::getInstance()->getTexture(ROAD_TEXT_PATH);
    }
    if(isPylon)
    {
      contents.push_back(new Pylon(3.0f, 0, 0, this)); // Place a pylon on the right
    }
}
/* DECONSTRUCTOR */
RoadTile::~RoadTile(){}
/* Draws the terrain - in this case the road */
void RoadTile::terrainDraw(float speed, float delta) // No LOD effects here as it is a key element of the scene
{    
    //glEnable(GL_COLOR_MATERIAL);
    glMaterialfv(GL_FRONT, GL_AMBIENT, roadAmbient);
    glMaterialfv(GL_FRONT, GL_DIFFUSE, roadDiffuse);
    glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, roadSpecular);
    glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, roadShine);
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, roadTexture);
    // One side of the roadTexture - Left Shoulder
    glBegin(GL_QUADS);
        glNormal3f(-0.069f, 0.997f, 0);glTexCoord2f(0,0); glVertex3f(-2.5f, 0, 2.5f); // Bottom Corner
        glNormal3f(-0.069f, 0.997f, 0);glTexCoord2f(0.25f,0);glVertex3f(-2.0f, 0.035f, 2.5f);
        glNormal3f(-0.069f, 0.997f, 0);glTexCoord2f(0.25f, 1); glVertex3f(-2.0f, 0.035f, -2.5f); // Top Corner
        glNormal3f(-0.069f, 0.997f, 0);glTexCoord2f(0,1); glVertex3f(-2.5f, 0, -2.5f);
    glEnd();
    //Left Middle/
    glBegin(GL_QUADS);
        glNormal3f(0, 1, 0);glTexCoord2f(0.25f, 0);glVertex3f(-2.0f, 0.035f, 2.5f);
        glNormal3f(0, 1, 0);glTexCoord2f(0.5f,0); glVertex3f( 0, 0.05f,  2.5f );
        glNormal3f(0, 1, 0);glTexCoord2f(0.5f, 1); glVertex3f(  0, 0.05f,  -2.5f );
        glNormal3f(0, 1, 0);glTexCoord2f(0.25f, 1); glVertex3f(  -2.0f, 0.035f, -2.5f );
    glEnd();
    // Right Shoulder - Middle
    glBegin(GL_QUADS);
        glNormal3f(0, 1, 0);glTexCoord2f(0.5f, 0);glVertex3f(0, 0.05f, 2.5f);
        glNormal3f(0, 1, 0);glTexCoord2f(0.75f,0); glVertex3f( 2.0f, 0.035f,  2.5f );
        glNormal3f(0, 1, 0);glTexCoord2f(0.75f, 1); glVertex3f(  2.0f, 0.035f,  -2.5f );
        glNormal3f(0, 1, 0);glTexCoord2f(0.5f, 1); glVertex3f(  0, 0.05f, -2.5f );
    glEnd();
    glBegin(GL_QUADS);
        glNormal3f(0.069f, 0.997f, 0);glTexCoord2f(0.75f, 0);glVertex3f(2.0f, 0.035f, 2.5f);
        glNormal3f(0.069f, 0.997f, 0);glTexCoord2f(1,0); glVertex3f( 2.5f, 0,  2.5f );
        glNormal3f(0.069f, 0.997f, 0);glTexCoord2f(1, 1); glVertex3f(  2.5f, 0,  -2.5f );
        glNormal3f(0.069f, 0.997f, 0);glTexCoord2f(0.75f, 1); glVertex3f(  2.0f, 0.035f, -2.5f );
    glEnd();
    
}
/**
 * contentsDraw
 * Draws the pylons that follow the road
 */
void RoadTile::contentsDraw(float speed, float delta)
{
    std::list<SceneObject*>::iterator iter;
    for(iter = contents.begin(); iter != contents.end(); iter++)
    {
        (*iter)->render(speed, delta);
    }
}