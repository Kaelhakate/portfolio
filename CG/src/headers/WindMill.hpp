/*
    WindMill
    A representation of a WindMill
    Author: Alex Peirce - 18859407
*/

#ifndef HAS_WMILL

#include "SceneObject.hpp"
#define HAS_WMILL


class WindMillTop; // Forward declaration

class WindMill:public SceneObject
{
    private:
        static const Mesh* mesh_lod_1;
        static const Mesh* mesh_lod_2;
        static GLuint wmillTexture;
        static const GLfloat wmillAmbient[4] ;
        static const GLfloat wmillDiffuse[4];
        static const GLfloat wmillSpecular[4];
        static const GLfloat wmillShininess;
        WindMillTop* topSection;
        void draw(float speed, float delta);
    public:
        WindMill();
        WindMill(float inx, float iny, float inz, SceneObject* parent);
        virtual ~WindMill();
};


#endif