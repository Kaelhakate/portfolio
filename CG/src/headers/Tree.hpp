/*
    Tree
    Represents a tree in the opengl
    Author: Alex Peirce - 18859407
*/

#ifndef HAS_TREE
#define HAS_TREE
#include "SceneObject.hpp"

class Tree:public SceneObject
{
    private:
        static bool HAS_TEXT;
        static GLuint textures[2];
        
        void draw(float speed, float delta);
	
	static const GLfloat treeAmbient[4];
	static const GLfloat treeDiffuse[4];
	static const GLfloat treeSpecular[4];
	static const GLfloat treeShininess;
	
	int selectedText;
    public:
        Tree();
        Tree(float, float, float, SceneObject* parent);
};
#endif