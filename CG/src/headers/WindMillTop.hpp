/*
    WindmillTop
    The top section of the windmill
    Author: Alex Peirce - 18859407
*/
#ifndef HAS_WIND_MILL_TOP
#define HAS_WIND_MILL_TOP

#include "SceneObject.hpp"

class WindMill;
class WindMillWheel;

class WindMillTop:public SceneObject
{
    private:
        static const Mesh* lod_1_mesh;
        static const Mesh* lod_2_mesh;
        WindMillWheel* wheelSection;
    public:
        WindMillTop(float x, float y, float z, WindMill* inparent);
        ~WindMillTop();
        
        void draw(float speed, float delta);
};
#endif