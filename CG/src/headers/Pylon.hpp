/*
  pylon
  Models a power pylon in the scene
  Author: Alex Peirce - 18859407
*/
#ifndef HAS_PYLON
#define HAS_PYLON
#include "SceneObject.hpp"

class Pylon: public SceneObject
{
  private:
    static GLuint pylonTexture;
    static const Mesh* lod_0_mesh;
    static const Mesh* lod_1_mesh;
    static const Mesh* lod_2_mesh;
    static const GLfloat pylonAmbient[4];
    static const GLfloat pylonDiffuse[4];
    static const GLfloat pylonSpecular[4];
    static const GLfloat pylonShininess;
    
    void draw(float speed, float delta);
  public:
    Pylon(float x, float y, float z, SceneObject* parent);
    ~Pylon();
};
#endif