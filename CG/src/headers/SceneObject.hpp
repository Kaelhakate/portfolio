/*
 * SceneObject
 * The base class for all models visible in the scene
 * Author: Alex Peirce - 18859407
 */
#ifndef HAS_SCENEOBJ
#define HAS_SCENEOBJ

#include "Vector3.hpp"
#include <cstdlib>
#include "MeshStore.hpp"
#include "TextureStore.hpp"
#include "Camera.hpp"

enum LOD{NOT_DEF,NONE,LOD_0, LOD_1, LOD_2};

class SceneObject
{  
    private:
        float deltaAcc;
        bool inSight;	
    protected:
        virtual void draw(float speed, float delta) = 0;
        
        LOD lod;
        Vector3* position;
        Vector3* scale;
        Vector3* rotation;
        SceneObject* parent;
	
	static const GLfloat defaultAmb[4];
	static const GLfloat defaultDiff[4];
	static const GLfloat defaultSpec[4];
	static const GLfloat defaultShine;
            
    public:
        SceneObject();
        SceneObject(float inx, float iny, float inz, SceneObject* inParent);
        virtual ~SceneObject();
                                
        void render(float speed, float delta);
        Vector3* const getPosition();
        Vector3* getGlobalPosition();    
        void movePosition(float x, float y, float z);
        void setScale(float x, float y, float z);
        void setPosition(float x, float y, float z);
        void rotateX(float inx);
        void rotateY(float iny);
        void rotateZ(float inz);
        bool isInSight();
        void setIsInSight(bool sightable);
        void determineLOD(); // To be called when needed
};
#endif