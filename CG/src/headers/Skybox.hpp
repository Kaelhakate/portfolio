/*
    Skybox
    Provides a background image to the scene.
    Adapted from here https://sidvind.com/wiki/Skybox_tutorial
    As my intial idea failed using gluSphere
    Author: Alex Peirce - 18859407
*/
#ifndef HAS_SKYBOX
#define HAS_SKYBOX
#include<string>
#include <exception>
#include <stdexcept>
#include "SceneObject.hpp"
#include "Camera.hpp"

#define SKY_FRONT 0
#define SKY_BACK 1
#define SKY_LEFT 2
#define SKY_RIGHT 3
#define SKY_TOP 4
#define SKY_BOTTOM 5

#define SKY_SIDES 6

class Skybox : public SceneObject
{
    private:
        Camera* cam;
        static GLuint skyText[SKY_SIDES];
        
        void loadTextures();
        
        
        void draw(float speed,float delta);
        void drawFront();
        void drawBack();
        void drawLeft();
        void drawRight();
        void drawTop();
        void drawBottom();
    public:
        Skybox(Camera* inCam, SceneObject* parent);
        ~Skybox();
};

#endif