/*
    Mesh
    Stores the information regarding a mesh loaded from a file
    Author: Alex Peirce - 18859407
*/
#ifndef HAS_MESH
#define HAS_MESH
#ifndef HAS_GL
#define GL_GLEXT_PROTOTYPES
#include <GL/gl.h>
#include <GL/glut.h>
#include <GL/glx.h>
#include <GL/glext.h>

// These are defined as attributes i put into the shader loader
#define VAO_VERT 0
#define VAO_NORMAL 1
#define VAO_UV 2

#define HAS_GL
#endif

#include <vector>
#include <fstream>
#include <sstream>
#include <string>
#include <stdio.h>
#include <iostream>

class Vertex
{
    public:
        float x,y,z;
};
typedef Vertex VertexNormal;
typedef Vertex FaceNormal;
class TextureCoord
{
    public:
        float u,v;
};
class Face
{
    public:
        int vertexCount;
        std::vector<Vertex*> verts;
        std::vector<VertexNormal*> vertNormals;
        std::vector<TextureCoord*> textureCoords;
};
class Mesh
{
    private:
        std::vector<Vertex*> verts;
        std::vector<VertexNormal*> vertNormals;
        std::vector<TextureCoord*> textureCoords;
        std::vector<Face*> faces;
        bool loadOBJ(const char* target);
    public:
        Mesh(const char* target);
        ~Mesh();
        
        void draw() const;
};
#endif