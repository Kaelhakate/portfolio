/*
    TextureStore
    Loads in images and gives requesting proccesses a handle to that image in texture form
    Author: Alex Peirce - 18859407
*/
#ifndef HAS_TEXTURE_STORE
#define HAS_TEXTURE_STORE

#ifndef HAS_GL
#define GL_GLEXT_PROTOTYPES
#include <GL/gl.h>
#include <GL/glut.h>
#include <GL/glx.h>
#include <GL/glext.h>

#define VAO_VERT 0
#define VAO_RGB 1
#define VAO_UV 2

#define HAS_GL
#endif

#include <map>
#include <string>
#include <exception>
class TextureStore
{
    private:
        static TextureStore* instance;
        std::map<int, GLuint> textures;
        GLuint loadTexture(char const* target);
        int hashTarget(char const* target);
    public:
        TextureStore();
        ~TextureStore();
        static TextureStore* getInstance();
        GLuint getTexture(char const* target);
};
#endif