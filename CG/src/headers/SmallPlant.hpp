/*
    SmallPlant
    Models the small plants used
    Author: Alex Peirce - 18859407
*/

#ifndef HAS_SMALL_PLANT
#define HAS_SMALL_PLANT
#include "SceneObject.hpp"
#include <cstdlib>

class SmallPlant: public SceneObject
{
    private:
        static bool HAS_TEXT;
        static GLuint textures[9];
	static const GLfloat plantAmbient[];
	static const GLfloat plantDiffuse[];
	static const GLfloat plantSpecular[];
	static const GLfloat plantShininess;
	
        int selectedText;
        void draw(float speed, float delta);
        void loadTextures();
    public:
        SmallPlant();
        SmallPlant(float inx, float iny, float inz, SceneObject* parent);
        ~SmallPlant();
};
#endif