/*
    GrassTile
    Non Road tiles will be grass tiles
    Author: Alex Peirce - 18859407
*/

#ifndef HAS_GRASS_TILE
#define HAS_GRASS_TILE

#include "TerrainTile.hpp"
#include "Tree.hpp"
#include "SmallPlant.hpp"
#include "WindMill.hpp"
#include "Shed.hpp"

#define GRASS_TEXT_PATH "textures/terrain/grass.jpg"
#define MAX_CONTENTS 25

class GrassTile:public TerrainTile
{
    private:
        static GLuint grassTexture;
        void terrainDraw(float speed, float delta);
        void contentsDraw(float speed, float delta);
        void genContents();
        void placeTree();
        void placePlant();
        void placeWindMill();
	
	static const GLfloat grassAmbient[4];
	static const GLfloat grassDiffuse[4];
	static const GLfloat grassSpecular[4];
	static const GLfloat grassShininess;
	
    public:
        GrassTile();
        GrassTile(float inx, float iny, float inz, SceneObject* parent);
        ~GrassTile();
};
#endif
