/*
    Tile
    Base Tile to be used in the scene
    Author: Alex Peirce - 18859407
*/
#ifndef HAS_TERRAIN_TILE
#define HAS_TERRAIN_TILE

#include "SceneObject.hpp"
#include <list>

class TerrainTile: public SceneObject
{
    private:
        void draw(float speed, float delta);
    protected:
        std::list <SceneObject*> contents;
        virtual void terrainDraw(float speed, float delta) = 0;
        virtual void contentsDraw(float speed, float delta) = 0;
    public:
        TerrainTile();
        TerrainTile(float inx, float iny, float inz, SceneObject* parent);
        virtual ~TerrainTile();
};
#endif