/*
 * Vector3
 * Defines a 3 dimensional vector
 * Author: Alex Peirce - 18859407
 */
#ifndef VECTOR_THREE
#define VECTOR_THREE
#include <math.h>
class Vector3{
    private:

    public:
        float x;
        float y;
        float z;
        Vector3();
        Vector3(float inx, float iny, float inz);
        ~Vector3();
        
        float getMagnitude();
        float getSqrMagnitude();
        static Vector3* normalize(Vector3* target);
        static Vector3* crossProd(Vector3* a, Vector3* b);
        
        Vector3* operator-(const Vector3 &other);
        Vector3& operator+=(const Vector3 &other);
};
#endif