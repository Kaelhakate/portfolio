/*
    MeshStore
    Maintains the storage of mesh objects.
    NOTE: The reason why this seems similar to TextureStore is because I simply retooled it for meshes
    Author: Alex Peirce - 18859407
*/
#ifndef HAS_MESH_STORE
#define HAS_MESH_STORE

#include <map>
#include <string>
#include <exception>

#include "Mesh.hpp"

class MeshStore
{
    private:
        static MeshStore* instance;
        std::map<int, Mesh*> meshes;
        
        int hashTarget(char const* target);
        
    public:
        MeshStore();
        ~MeshStore();
        static MeshStore* getInstance();
        
        const Mesh* getMesh(char const* file);
};
#endif