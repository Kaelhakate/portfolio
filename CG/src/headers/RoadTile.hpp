/*
    RoadTile
    Models the road in the scene and the associated models
    Author: Alex Peirce - 18859407
*/
#ifndef HAS_ROAD_TILE
#define HAS_ROAD_TILE

#include "TerrainTile.hpp"
#define ROAD_TEXT_PATH "textures/terrain/road.jpg"
#include "Pylon.hpp"

class RoadTile: public TerrainTile
{
    private:
        static GLuint roadTexture;
        static GLfloat roadAmbient[4];
        static GLfloat roadDiffuse[4];
        static GLfloat roadSpecular[4];
        static GLfloat roadShine;
        
        void terrainDraw(float speed, float delta);
        void contentsDraw(float speed, float delta);
    public:
        RoadTile();
        RoadTile(float, float, float, SceneObject* parent, bool isPylon);
        ~RoadTile();
};
#endif