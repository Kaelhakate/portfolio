/*
    Camera
    Used to store the camera's state
    Author: Alex Peirce - 18859407
*/

#ifndef HAS_CAMERA
#define HAS_CAMERA
#include "Vector3.hpp"
#define CAMERA_DEFAULT_FOV 60.0
#define CAMERA_DEFAULT_ASPECT 16/9
#define CAMERA_NEAR 0.1f
#define CAMERA_FAR 200.0f
#define CAMERA_SQR_FAR 40000
#include <cstddef> // NULL definition
#include <cmath>

class Camera
{
        private:
            static Camera* mainCamera;                    
            Vector3* camFront;
            Vector3* position;
            Vector3* camUp;
            
            Vector3* directionFront;
            void updateDirectionVector();
            float fov;
            float aspect;
        public:
            Camera();
            Camera(float x, float y, float z);
            ~Camera();
            
            static const float zNear = CAMERA_NEAR;
            static const float zFar = CAMERA_FAR;
            void setTarget(Vector3* target);
            void movePosition(float x, float y, float z);
            void setPosition(float x, float y, float z);
            const Vector3* getPosition();
            const Vector3* getCamUp();
            const Vector3* getCamFront();
            static void makeMainCamera(Camera* cam);
            static Camera* getMainCamera();
            bool checkIfInSight(const Vector3* point);
            float getFOV();
            void setFOV(float infov);
            float getAspect();
            void setAspect(float inaspect);
};
#endif
