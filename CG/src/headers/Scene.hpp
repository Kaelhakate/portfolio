/*
    Scene
    Extends SceneObject to represent a collection of SceneObjects that make up a scene
    Author: Alex Peirce - 18859407
*/

#ifndef HAS_GL
#include <GL/gl.h>
#include <GL/glut.h>
#define HAS_GL
#endif

#include <list>

#include "SceneObject.hpp"
#include "Camera.hpp"
#include "Skybox.hpp"
#include "TerrainTile.hpp"
#include "GrassTile.hpp"
#include "RoadTile.hpp"
#include "WindMill.hpp"


class Scene: public SceneObject
{
    private:
        Skybox *sky;
        std::list<SceneObject*> objList;
        std::list<TerrainTile*> terrainList;
        
        void updateTerrain(float speed, float delta);
    public:
        Scene(Camera* inCam);
        ~Scene();
        void draw(float speed, float delta);
};