/*
    WindmillWheel
    The Wheel section of the windmill
    Author: Alex Peirce - 18859407
*/
#ifndef HAS_WIND_MILL_WHEEL
#define HAS_WIND_MILL_WHEEL

#include "SceneObject.hpp"

class WindMillTop; // Forward declaration

class WindMillWheel:public SceneObject
{
    private:
        static const Mesh* lod_1_mesh;
        static const Mesh* lod_2_mesh;
        int rotationSpeed;
    public:
        WindMillWheel(float x, float y, float z, WindMillTop* parent);
        ~WindMillWheel();
        
        void draw(float speed, float delta);
};
#endif