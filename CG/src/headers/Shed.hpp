/*
    Shed
    Models a rusty shed in the scene
    Author: Alex Peirce - 18859407
*/

#ifndef HAS_SHED
#define HAS_SHED
#include "SceneObject.hpp"
class Shed: public SceneObject
{
    private:
        static GLuint shedTexture;
        static const Mesh* lod_0_mesh;
        static const Mesh* lod_1_mesh;
        static const Mesh* lod_2_mesh;
        static const GLfloat shedAmbient[4];
        static const GLfloat shedDiffuse[4];
        static const GLfloat shedSpecular[4];
        static const GLfloat shedShininess;
        
        
        void draw(float speed, float delta);
    public:
        Shed(float x, float y, float z, SceneObject* parent);
        ~Shed();
};
#endif