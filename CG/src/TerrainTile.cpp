#include "headers/TerrainTile.hpp"

/* CONSTRUCTOR */
TerrainTile::TerrainTile(){}
/* ALTERNATE CONSTRUCTOR */
TerrainTile::TerrainTile(float inx, float iny, float inz, SceneObject* parent):SceneObject(inx, iny, inz,parent){}
/* DECONSTRUCTOR */
TerrainTile::~TerrainTile()
{
    std::list<SceneObject*>::iterator itr;
    for(itr = contents.begin();itr != contents.end(); itr++)
    {
        delete (*itr);
    }
    contents.clear();
}
/*
    draw
    Draws the terrain and then its contents
    IMPORT:
        float - speed - Rate animations should run at
        float - delta - Time since the last frame
*/
void TerrainTile::draw(float speed, float delta)
{
    terrainDraw(speed, delta);
    contentsDraw(speed, delta);
}