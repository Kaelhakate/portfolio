#include "headers/Vector3.hpp"
Vector3::Vector3()
{
    x = 0.0f;
    y = 0.0f;
    z = 0.0f;
}
Vector3::Vector3(float inx, float iny, float inz)
{
    x = inx;
    y = iny;
    z = inz;
}
Vector3::~Vector3(){}

float Vector3::getSqrMagnitude()
{
    return (x*x) + (y*y) + (z*z);
}
float Vector3::getMagnitude()
{
    return sqrtf((x*x) + (y*y) + (z*z));
}
Vector3* Vector3::normalize(Vector3* target)
{
    float mag = target->getMagnitude();
    float outx = target->x;
    float outy = target->y;
    float outz = target->z;
    outx /= mag;
    outy /= mag;
    outz /= mag;
    return new Vector3(outx,outy,outz);
}

Vector3* Vector3::crossProd(Vector3* a, Vector3* b)
{
    float x = (a->y * b->z) - (a->z * b->y);
    float y = (a->z * b->x) - (a->x * b->z);
    float z = (a->x * b->y) - (a->y * b->x);
    return new Vector3(x, y, z);
}

Vector3* Vector3::operator-(const Vector3 &other)
{
    return new Vector3(x - other.x, y  - other.y, z - other.z);
}

Vector3& Vector3::operator+=(const Vector3 &other)
{
    this->x += other.x;
    this->y += other.y;
    this->z += other.z;
    return *this;
}