#include "headers/Mesh.hpp"
/* DEFAULT CONSTRUCTOR */
Mesh::Mesh(const char* target)
{
    loadOBJ(target);
}
/* DECONSTRUCTOR */
Mesh::~Mesh()
{
    std::vector<Face*>::iterator fit;
    std::vector<Vertex*>::iterator vit;
    std::vector<VertexNormal*>::iterator nit;
    std::vector<TextureCoord*>::iterator tcit;
    
    for(vit = verts.begin(); vit != verts.end(); vit++)
    {
        delete (*vit);
    }
    for(nit = vertNormals.begin(); nit != vertNormals.end(); nit++)
    {
        delete (*nit);
    }
    for(tcit = textureCoords.begin(); tcit != textureCoords.end(); tcit++)
    {
        delete (*tcit);
    }
    for(fit = faces.begin(); fit != faces.end(); fit++)
    {
        delete (*fit);
    }
}
/*
    draw
    Draws the contents of the mesh
*/
void Mesh::draw() const
{
    std::vector<Face*>::const_iterator it;
    std::vector<Vertex*>::const_iterator vit;
    std::vector<VertexNormal*>::const_iterator nit;
    std::vector<TextureCoord*>::const_iterator tcit;
    Face* face;
    GLenum mode;
    
    for(it = faces.begin(); it != faces.end(); it++)
    {
        face = (*it);
        if(face->vertexCount == 3)
        {
            glBegin(GL_TRIANGLES);
            for(int ii = 0; ii < 3; ii++)
            {
                glTexCoord2f(face->textureCoords[ii]->u, face->textureCoords[ii]->v);
                glNormal3f(face->vertNormals[ii]->x, face->vertNormals[ii]->y, face->vertNormals[ii]->z);
                glVertex3f(face->verts[ii]->x, face->verts[ii]->y, face->verts[ii]->z);
            }
        }
        else if(face->vertexCount == 4)
        {
            glBegin(GL_QUADS);
            for(int ii = 0; ii < 4; ii++)
            {
                glTexCoord2f(face->textureCoords[ii]->u, face->textureCoords[ii]->v);
                glNormal3f(face->vertNormals[ii]->x, face->vertNormals[ii]->y, face->vertNormals[ii]->z);
                glVertex3f(face->verts[ii]->x, face->verts[ii]->y, face->verts[ii]->z);
            }
        }
        else
        {
            glBegin(GL_POLYGON);
            for(int ii = 0; ii < face->vertexCount; ii++)
            {
                glTexCoord2f(face->textureCoords[ii]->u, face->textureCoords[ii]->v);
                glNormal3f(face->vertNormals[ii]->x, face->vertNormals[ii]->y, face->vertNormals[ii]->z);
                glVertex3f(face->verts[ii]->x, face->verts[ii]->y, face->verts[ii]->z);
            }
        }
        glEnd();
    }
}
/*
    loadOBJ
    loads in a obj file's data. Adapted from here
    https://en.wikibooks.org/wiki/OpenGL_Programming/Modern_OpenGL_Tutorial_Load_OBJ
*/    
bool Mesh::loadOBJ(const char* target)
{
    bool succesful = false;
    std::string line;
    std::string token;
    Vertex* v;
    VertexNormal* vn;
    TextureCoord* vt;
    Face* f;
    
    int vi,ti,ni;
    
    std::cerr << "Loading Mesh: " << target;
    std::ifstream in(target, std::ios::in); // Open the file for reading
    if(in)
    {
        while(std::getline(in, line)) // Read each line until no more
        {
            if(line[0] == 'v' && line[1] == ' ') // if a vertex
            {
                std::istringstream s(line.substr(2));
                v = new Vertex();
                s >> v->x; s >> v->y; s >> v->z; // Loadi ngin the values off the vertex spec in the file
                verts.push_back(v);                
            }
            else if(line[0] == 'v' && line[1] == 'n')// If a vertex normal
            {
                std::istringstream s(line.substr(3));
                vn = new VertexNormal();
                s >> vn->x; s >> vn->y; s >> vn->z; // The normals
                vertNormals.push_back(vn);
            }
            else if(line[0] == 'v' && line[1] == 't') // if a texture coord
            {
                std::istringstream s(line.substr(3));
                vt = new TextureCoord();
                s >> vt->u; s >> vt->v;
                textureCoords.push_back(vt);
            }
            else if(line[0] == 'f' && line[1] == ' ')// if a face
            {
                std::istringstream s(line.substr(2));
                f = new Face();
                f->vertexCount = 0;
                while(std::getline(s, token, ' '))
                {
                    vi = -1; ti = -1; ni = -1;
                    if(sscanf(token.c_str(), "%d/%d/%d", &vi, &ti, &ni) == 3)
                    {
                        // Indexes are a needing to be set back by one as the file isnt 0 based
                        f->verts.push_back(verts[vi-1]); // Add the vertex
                        f->textureCoords.push_back(textureCoords[ti - 1]); // UV coords
                        f->vertNormals.push_back(vertNormals[ni - 1]); // The normal for that vertex;
                        (f->vertexCount)++;                        
                    }
                }
                faces.push_back(f);
            }
        }
        in.close();
        succesful = true;
        std::cerr << " Success! "<< faces.size() << " Faces (" << verts.size() << " vertices, "<< vertNormals.size() <<" normals, " << textureCoords.size() <<" uv coords)\n";
    }
    else
    {
        std::cerr << "Failed!\n";
    }
    return succesful; // While not entirely true if a vertex was not found but indicates the file was opened
}