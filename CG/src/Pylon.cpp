#include "headers/Pylon.hpp"

GLuint Pylon::pylonTexture= 0;
const Mesh* Pylon::lod_0_mesh = NULL;
const Mesh* Pylon::lod_1_mesh = NULL;
const Mesh* Pylon::lod_2_mesh = NULL;

const GLfloat Pylon::pylonAmbient[4] = {0.2, 0.125, 0.125, 1.0};
const GLfloat Pylon::pylonDiffuse[4] = {0.2, 0.2, 0.2, 1.0};
const GLfloat Pylon::pylonSpecular[4] = {0.3, 0.25, 0.15, 1.0};
const GLfloat Pylon::pylonShininess = 0.2f;

/* CONSTRUCTOR */
Pylon::Pylon(float x, float y, float z, SceneObject* parent):SceneObject(x,y,z,parent)
{
    if(lod_0_mesh == NULL)
    {
        lod_0_mesh = MeshStore::getInstance()->getMesh("models/pylon_0.obj");
    }
    if(lod_1_mesh == NULL)
    {
        lod_1_mesh = MeshStore::getInstance()->getMesh("models/pylon_1.obj");
    }    
    if(lod_2_mesh == NULL)
    {
        lod_2_mesh = MeshStore::getInstance()->getMesh("models/pylon_2.obj");
    }
    if(pylonTexture == 0)
    {
         pylonTexture = TextureStore::getInstance()->getTexture("textures/pylon.jpg");
    }
}
/* DECONSTRUCTOR */
Pylon::~Pylon(){}

void Pylon::draw(float speed, float delta)
{
    glMaterialfv(GL_FRONT, GL_AMBIENT, pylonAmbient);
    glMaterialfv(GL_FRONT, GL_DIFFUSE, pylonDiffuse);
    glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, pylonSpecular);
    glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, pylonShininess);
    
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, pylonTexture);
    if(lod == LOD_0)
    {
        lod_0_mesh->draw();
    }
    else if(lod == LOD_1)
    {
        lod_1_mesh->draw();
    }
    else
    {
        lod_2_mesh->draw();
    }
    glEnable(GL_LINE_SMOOTH);
    glLineWidth(1.5f);
    glBegin(GL_LINE_STRIP);
      glVertex3f(0.75f,5.5f,0);
      glVertex3f(0.75f,5.4f,3.0f);
      glVertex3f(0.75f,5.35f,7.5f);
      glVertex3f(0.75f,5.4f,12.0f);
      glVertex3f(0.75f,5.5f, 15.0f);
    glEnd();
    glBegin(GL_LINE_STRIP);
      glVertex3f(-0.75f,5.5f,0);
      glVertex3f(-0.75f,5.4f,3.0f);
      glVertex3f(-0.75f,5.35f,7.5f);
      glVertex3f(-0.75f,5.4f,12.0f);
      glVertex3f(-0.75f,5.5f, 15.0f);
    glEnd();
    glLineWidth(1.0f);
    glDisable(GL_LINE_SMOOTH);
    
    glDisable(GL_TEXTURE_2D);
    glMaterialfv(GL_FRONT, GL_AMBIENT, defaultAmb);
    glMaterialfv(GL_FRONT, GL_DIFFUSE, defaultDiff);
    glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, defaultSpec);
    glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, defaultShine); 
}