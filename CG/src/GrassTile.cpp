#include "headers/GrassTile.hpp"
GLuint GrassTile::grassTexture = 0;
const GLfloat GrassTile::grassAmbient[4] = {0.1, 0.15, 0.1, 1.0};
const GLfloat GrassTile::grassDiffuse[4] = {0.45, 0.43, 0.33, 1.0};
const GLfloat GrassTile::grassSpecular[4] = {0.1, 0.1, 0.1, 1.0};
const GLfloat GrassTile::grassShininess = 0.1f;
/* DEFAULT CONSTRUCTOR */
GrassTile::GrassTile()
{
    if(grassTexture == 0)
    {
        grassTexture = TextureStore::getInstance()->getTexture(GRASS_TEXT_PATH);
    }
    genContents();
}
/* ALT CONSTRUCTOR */
GrassTile::GrassTile(float inx, float iny, float inz, SceneObject* parent):TerrainTile(inx,iny,inz, parent)
{
    if(grassTexture == 0)
    {
        grassTexture = TextureStore::getInstance()->getTexture(GRASS_TEXT_PATH);
    }
    genContents();
}
/* DECONSTRUCTOR */
GrassTile::~GrassTile()
{
}
/*
 * terrainDraw
 * The main call to draw the terrain plane
 * IMPORT:
 *    float - speed - Rate animations should run at
 *    float - delta - The time since the last frame
 */
void GrassTile::terrainDraw(float speed, float delta)
{
    glMaterialfv(GL_FRONT, GL_AMBIENT, grassAmbient);
    glMaterialfv(GL_FRONT, GL_DIFFUSE, grassDiffuse);
    glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, grassSpecular);
    glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, grassShininess);
    if(lod == LOD_2)
    {
        glEnable(GL_TEXTURE_2D);
        glBindTexture(GL_TEXTURE_2D, grassTexture);
        glBegin(GL_QUADS);
            glNormal3f(0,1,0);glTexCoord2f(0, 0); glVertex3f(-2.5f, 0, 2.5f);
            glNormal3f(0,1,0);glTexCoord2f(0, 1); glVertex3f( 2.5f, 0,  2.5f );
            glNormal3f(0,1,0);glTexCoord2f(1, 1); glVertex3f(  2.5f, 0,  -2.5f );
            glNormal3f(0,1,0);glTexCoord2f(1, 0); glVertex3f(  -2.5f, 0, -2.5f );
        glEnd();
        glDisable(GL_TEXTURE_2D);
    }
    else // Draw a greeny brownish non textured tile
    {
        glColor3f(0.4f, 0.4f, 0.2f);
        glBegin(GL_QUADS);
            glNormal3f(0,1,0); glVertex3f(-2.5f, 0, 2.5f);
            glNormal3f(0,1,0); glVertex3f( 2.5f, 0,  2.5f );
            glNormal3f(0,1,0); glVertex3f(  2.5f, 0,  -2.5f );
            glNormal3f(0,1,0); glVertex3f(  -2.5f, 0, -2.5f );
        glEnd();
        glColor3f(1, 1, 1); // reset to white
    }
    /* Revoke Material Bindings */
    glMaterialfv(GL_FRONT, GL_AMBIENT, defaultAmb);
    glMaterialfv(GL_FRONT, GL_DIFFUSE, defaultDiff);
    glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, defaultSpec);
    glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, defaultShine);  
}
/*
 * contentsDraw
 * Draws the current objects in the terrain's "space"
 */
void GrassTile::contentsDraw(float speed, float delta)
{
    std::list<SceneObject*>::iterator iter;
    for(iter = contents.begin(); iter != contents.end(); iter++)
    {
        (*iter)->render(speed, delta);
    }
}
/*
 * genContents
 * Generates randomly the objects on the terrain
 */
void GrassTile::genContents()
{
    bool windMill = false;
    bool shed = false;
    int temp;
    for(int ii= 0; ii < MAX_CONTENTS; ii++)
    {
        temp = rand()%1000;
        if(temp == 0 && !windMill && !shed) // 0.01% of a windmill
        {
            placeWindMill();
            windMill = true;
        }
        else if (temp == 1 && !windMill && !shed) // same for a rundown shed
        {
            contents.push_back(new Shed(0,0,0,this));
            shed = true;
        }
        else if(temp < 100) // 10% chance of a tree
        {
            placeTree();
        }
        else if(temp < 800) // 70% chance of a plant
        {
            placePlant();
        }
        // 20 % chance of nothing
    }
}
/* Place a tree on the grass tile randomly */
void GrassTile::placeTree()
{
    float tX = ((rand() % 100 + 1) /23.26) - 2.2f;
    float tZ = ((rand() % 100 + 1) /23.26) - 2.2f;
    Tree * t = new Tree(tX, 0, tZ, this);
    contents.push_back(t);
}
/* Places a small plant on the grass tile randomly */
void GrassTile::placePlant()
{
    float pX = ((rand() % 100 + 1) /21.75) - 2.3f;
    float pZ = ((rand() % 100 + 1) /21.75) - 2.3f;
    SmallPlant * p = new SmallPlant(pX, 0, pZ, this);
    contents.push_back(p);
}
/* Place a windmill down some where on the tile */
void GrassTile::placeWindMill()
{
    float wX = ((rand() % 100 + 1) / 40.00) - 1.25f;
    float wZ = ((rand() % 100 + 1) / 40.00) - 1.25f;
    WindMill *w = new WindMill(wX, 0, wZ, this);
    contents.push_back(w);
}
