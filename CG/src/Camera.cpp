#include "headers/Camera.hpp"
#define PI 3.14159265

Camera* Camera::mainCamera = 0;
/* DEFAULT CONSTRUCTOR */
Camera::Camera()
{
    position = new Vector3();
    camFront = new Vector3(0,0,-1);
    camUp = new Vector3(0.0f, 1.0f, 0.0f);
    if(mainCamera == 0)
    {
        mainCamera = this;
    }
    directionFront = NULL;
    fov = CAMERA_DEFAULT_FOV;
    aspect =  CAMERA_DEFAULT_ASPECT;
}
/* ALT CONSTRUCTOR - Places its transform in world space */
Camera::Camera(float x, float y, float z)
{
    position = new Vector3(x,y,z);
    camFront = new Vector3(0,0,-1);
    camUp = new Vector3(0.0f, 1.0f, 0.0f);
    if(mainCamera == 0)
    {
        mainCamera = this;
    }
    directionFront = NULL;
    fov = CAMERA_DEFAULT_FOV;
    aspect =  CAMERA_DEFAULT_ASPECT;
}
/* DECONSTRUCTOR */
Camera::~Camera()
{
    delete position;
    delete camFront;
    delete camUp;
    delete directionFront;
}
/*
    updateDirectionVector
    Calculates the direction vector for the cam position to the target
*/
void Camera::updateDirectionVector()
{
    directionFront->x = camFront->x - position->x;
    directionFront->y = camFront->y - position->y;
    directionFront->z = camFront->z - position->z;
    Vector3* temp = Vector3::normalize(directionFront);
    delete directionFront;
    directionFront = temp; // Now a unit vector
}
/* 
 * setTarget
 * Sets the camera's look target
 * IMPORT:
 *     Vector3* target The target to look at
 */
void Camera::setTarget(Vector3* target)
{
    camFront->x = target->x;
    camFront->y = target->y;
    camFront->z = target->z;
    updateDirectionVector();
}
/*
    movePosition
    Moves the position of the camera
    IMPORT:
      floatsx,y,z The position to move the camera to move to
*/
void Camera::movePosition(float x, float y, float z)
{
    position->x += x;
    position->y += y;
    position->z += z;
    updateDirectionVector();
}
/*
    setPostion
    Moves the object to a given coordinate
*/
void Camera::setPosition(float x, float y, float z)
{
    position->x = x;
    position->y = y;
    position->z = y;
    updateDirectionVector();
}
/*
 * makeMainCamera
 * Sets the camera to be the main camera
 * IMPORT:
 *     The camera to be set to main
 */
void Camera::makeMainCamera(Camera* cam)
{
    mainCamera = cam;
}
/*
 * getMainCamera
 * Returns the pointer to the main camera
 */
Camera* Camera::getMainCamera()
{
    return mainCamera;
}
/*
    checkIfInSight
    Very basic frustrum culling.
    This was due to the fact I had set all this up before knowing to stick in frustrum culling.
    However on further reading apparently opengl does perform this, but after the vertexes have been submitted to the gpu
    So in order to reduce load I have adopted a 180 degree boundary, so anything behind is culled,
    everything else has a chance of still being in the frustrum. So I have simplified it to be if thier
    root point was in this 180 degree view then draw and let opengl deal with it.
    Otherwise If i had the time for the refactoring I would have added the extra checks for partials and 
    done the proper frustrum culling before gpu submission
    A better explanation will be in the report
    IMPORT
        Vector3 - The point in world space to be checked
    EXPORT:
        bool - Confirmation if the point is "viewable" that is inside a 180 POV from the camear's look direction
*/

bool Camera::checkIfInSight(const Vector3* point)
{
    if(directionFront == NULL)
    {
        directionFront = new Vector3();
        updateDirectionVector();
    }
    Vector3* directionPoint = new Vector3(point->x - position->x, point->y - position->y,point->z - position->z);
    Vector3* temp = Vector3::normalize(directionPoint);
    delete directionPoint;
    directionPoint = temp; // Unit Vector
    float dot = ((directionFront->x * directionPoint->x) + (directionFront->y * directionPoint->y) + (directionFront->z * directionPoint->z));
    
    delete directionPoint;
    return dot > 0; // +5 degrees to give some leeway on objects that are large
}

// GETTERS/SETTERS
const Vector3* Camera::getPosition()
{
    return position;
}
const Vector3* Camera::getCamUp()
{
    return camUp;
}
const Vector3* Camera::getCamFront()
{
    return camFront;
}
float Camera::getFOV()
{
    return fov;
}

void Camera::setFOV(float infov)
{
    if(infov > 0)
    {
        fov = infov;
    }
}

float Camera::getAspect()
{
    return aspect;
}

void Camera::setAspect(float inaspect)
{
    if(inaspect > 0)
    {
        aspect = inaspect;
    }
}



