#include "headers/Skybox.hpp"
GLuint Skybox::skyText [SKY_SIDES]= {};
/* CONSTRUCTOR */
Skybox::Skybox(Camera* inCam, SceneObject* parent)
{
    cam = inCam;
    loadTextures();
    parent = parent;
}
/* DECONSTRUCTOR */
Skybox::~Skybox()
{
}
/*
    loadTextures
    Loads in the textures in TGA format in the given directory with a expect file names
*/
void Skybox::loadTextures()
{
    std::string dir = "textures/skybox/";
    std::string FRONT = "negz.jpg";
    std::string BACK = "posz.jpg";
    std::string LEFT = "posx.jpg";
    std::string RIGHT = "negx.jpg";
    std::string TOP = "posy.jpg";
    std::string BOTTOM = "negy.jpg";
    
    skyText[SKY_FRONT] = TextureStore::getInstance()->getTexture((dir+FRONT).c_str());
    skyText[SKY_BACK] = TextureStore::getInstance()->getTexture((dir+BACK).c_str());
    skyText[SKY_LEFT] = TextureStore::getInstance()->getTexture((dir+LEFT).c_str());
    skyText[SKY_RIGHT] = TextureStore::getInstance()->getTexture((dir+RIGHT).c_str());
    skyText[SKY_TOP] = TextureStore::getInstance()->getTexture((dir+TOP).c_str());
    skyText[SKY_BOTTOM] = TextureStore::getInstance()->getTexture((dir+BOTTOM).c_str());
}
/* 
 * draw
 * The draw method for the skybox, inherited from the sceneobject
 * IMPORT:
 *    float - speed - Rate animations should run at; Could be used to animate the skybox
 *    float - delta - Time since the last frame
 */
void Skybox::draw(float speed, float delta)
{
    
    glLoadIdentity();
    //gluLookAt(0,0,0,cam->position->x, cam->position->y, cam->position->z, 0,1,0);
    glPushAttrib(GL_ENABLE_BIT); // Get a new attribute state
    glEnable(GL_TEXTURE_2D);
    glDisable(GL_DEPTH_TEST); // No writing depth allows for skybox effect
    glDisable(GL_LIGHTING); // Skybox isnt suceptible to lighting
    glDisable(GL_BLEND);
    glDisable(GL_CULL_FACE);

    
    // Just fills in any gaps (if any)*/
    glColor3f(1.0f, 1.0f, 1.0f); //No need for transparency as it is disabled to begin with
    glEnable(GL_TEXTURE_2D);
    drawFront();
    drawBack();
    drawLeft();
    drawRight();
    drawTop();
    drawBottom(); 
    // Reset settings and Matrix
    glPopAttrib();
    glDisable(GL_TEXTURE_2D);
}

void Skybox::drawFront()
{
    
    glBindTexture(GL_TEXTURE_2D, skyText[SKY_FRONT]);
    glBegin(GL_QUADS);
        glTexCoord2f(0, 0); glVertex3f(  0.5f, -0.5f, -0.5f );
        glTexCoord2f(1, 0); glVertex3f( -0.5f, -0.5f, -0.5f );
        glTexCoord2f(1, 1); glVertex3f( -0.5f,  0.5f, -0.5f );
        glTexCoord2f(0, 1); glVertex3f(  0.5f,  0.5f, -0.5f );
    glEnd();
    
}
// DRAW SIDES FOLLOW ON FROM HERE
void Skybox::drawBack()
{
    glBindTexture(GL_TEXTURE_2D, skyText[SKY_BACK]);
    glBegin(GL_QUADS);
        glTexCoord2f(0, 0); glVertex3f( -0.5f, -0.5f,  0.5f );
        glTexCoord2f(1, 0); glVertex3f(  0.5f, -0.5f,  0.5f );
        glTexCoord2f(1, 1); glVertex3f(  0.5f,  0.5f,  0.5f );
        glTexCoord2f(0, 1); glVertex3f( -0.5f,  0.5f,  0.5f );
    glEnd();
}
void Skybox::drawLeft()
{
    glBindTexture(GL_TEXTURE_2D, skyText[SKY_LEFT]);
    glBegin(GL_QUADS);
        glTexCoord2f(0, 0); glVertex3f(  0.5f, -0.5f,  0.5f );
        glTexCoord2f(1, 0); glVertex3f(  0.5f, -0.5f, -0.5f );
        glTexCoord2f(1, 1); glVertex3f(  0.5f,  0.5f, -0.5f );
        glTexCoord2f(0, 1); glVertex3f(  0.5f,  0.5f,  0.5f );
    glEnd();
}

void Skybox::drawRight()
{
    glBindTexture(GL_TEXTURE_2D, skyText[SKY_RIGHT]);
    glBegin(GL_QUADS);
        glTexCoord2f(0, 0); glVertex3f( -0.5f, -0.5f, -0.5f );
        glTexCoord2f(1, 0); glVertex3f( -0.5f, -0.5f,  0.5f );
        glTexCoord2f(1, 1); glVertex3f( -0.5f,  0.5f,  0.5f );
        glTexCoord2f(0, 1); glVertex3f( -0.5f,  0.5f, -0.5f );
    glEnd();
}

void Skybox::drawTop()
{
    glBindTexture(GL_TEXTURE_2D, skyText[SKY_TOP]);
    glBegin(GL_QUADS);
        glTexCoord2f(0, 1); glVertex3f( -0.5f,  0.5f, -0.5f );
        glTexCoord2f(0, 0); glVertex3f( -0.5f,  0.5f,  0.5f );
        glTexCoord2f(1, 0); glVertex3f(  0.5f,  0.5f,  0.5f );
        glTexCoord2f(1, 1); glVertex3f(  0.5f,  0.5f, -0.5f );
    glEnd();
}

void Skybox::drawBottom()
{
    glBindTexture(GL_TEXTURE_2D, skyText[SKY_BOTTOM]);
    glBegin(GL_QUADS);
        glTexCoord2f(0, 0); glVertex3f( -0.5f, -0.5f, -0.5f );
        glTexCoord2f(0, 1); glVertex3f( -0.5f, -0.5f,  0.5f );
        glTexCoord2f(1, 1); glVertex3f(  0.5f, -0.5f,  0.5f );
        glTexCoord2f(1, 0); glVertex3f(  0.5f, -0.5f, -0.5f );
    glEnd();
}