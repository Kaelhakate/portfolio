#include "headers/WindMillWheel.hpp"
#include "headers/WindMillTop.hpp"
#include <cmath>

const Mesh* WindMillWheel::lod_1_mesh = NULL;
const Mesh* WindMillWheel::lod_2_mesh = NULL;

WindMillWheel::WindMillWheel(float x, float y, float z, WindMillTop* parent):SceneObject(x,y,z, parent)
{
    if(lod_1_mesh == NULL)
    {
        lod_1_mesh = MeshStore::getInstance()->getMesh("models/wmill_wheel_1.obj");
    }    
    if(lod_2_mesh == NULL)
    {
        lod_2_mesh = MeshStore::getInstance()->getMesh("models/wmill_wheel_2.obj");
    }
    rotationSpeed = rand() % 45 + 1; // degrees per second (randomized)
}

WindMillWheel::~WindMillWheel(){}

void WindMillWheel::draw(float speed, float delta)
{
    const int SIDES_CIRCLE = 12;
    const float RADIUS = 1.15;
    const float PI = 3.14159265; // Pi definition from http://www.cplusplus.com/reference/cmath/cos/
    if(lod == LOD_0)
    {
        float segmentAngle = 360 / SIDES_CIRCLE;
        float angle, x, y;
        
        glBegin(GL_TRIANGLE_FAN);
            glVertex3f(0,0,0); // Set up origin point
            for(int ii = 0; ii < SIDES_CIRCLE; ii++) // We want 12 triangles to keep detail down
            {
                angle = (ii * segmentAngle) * PI / 180.0;
                x = RADIUS * cos(angle);
                y = RADIUS * sin(angle);
                glVertex3f(x,y,0);
            }
            // To close the fan
            angle = (0*segmentAngle) * PI /180.0;
            x = RADIUS * cos(angle);
            y = RADIUS * sin(angle);
            glVertex3f(x,y,0);
        glEnd();
    }
    else if(lod == LOD_1)
    {
        lod_1_mesh->draw();
    }
    else
    {
        lod_2_mesh->draw();
    }
    this->rotateZ(rotationSpeed*speed*delta); // Rotate 2 deg per sec @ 1 speed preparing for the next draw
}