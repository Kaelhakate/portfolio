#include "headers/WindMillTop.hpp"
#include "headers/WindMill.hpp"
#include "headers/WindMillWheel.hpp"

const Mesh* WindMillTop::lod_1_mesh = NULL;
const Mesh* WindMillTop::lod_2_mesh = NULL;

WindMillTop::WindMillTop(float x, float y, float z, WindMill* inparent):SceneObject(x,y,z, inparent)
{
    if(lod_1_mesh == NULL)
    {
        lod_1_mesh = MeshStore::getInstance()->getMesh("models/wmill_top_1.obj");
    }    
    if(lod_2_mesh == NULL)
    {
        lod_2_mesh = MeshStore::getInstance()->getMesh("models/wmill_top_2.obj");
    }
    wheelSection = new WindMillWheel(0,0,0.3f, this);
    wheelSection->rotateZ(rand() % 360); // Give a random starting rotation
}

WindMillTop::~WindMillTop()
{
    delete wheelSection;
}

void WindMillTop::draw(float speed, float delta)
{
    const float BOX_WIDTH = 0.3f;
    const float BOX_HEIGHT = 0.3f;
    const float BOX_LENGTH = 0.55f;
    
    if(lod == LOD_0)
    { // Using Normals to overidde the defaults given by the order of vertexes
        // Also no drawing an entire box just the sides that would be visible at a distance
        glBegin(GL_QUADS); // TOP PLANE
            glNormal3f(0,1,0); glVertex3f(-BOX_WIDTH/2, BOX_HEIGHT/2, BOX_LENGTH / 2);
            glNormal3f(0,1,0); glVertex3f(BOX_WIDTH/2, BOX_HEIGHT/2, BOX_LENGTH / 2);
            glNormal3f(0,1,0); glVertex3f(BOX_WIDTH/2, BOX_HEIGHT/2, -BOX_LENGTH / 2);
            glNormal3f(0,1,0); glVertex3f(-BOX_WIDTH/2, BOX_HEIGHT/2, -BOX_LENGTH / 2);
        glEnd();
        glBegin(GL_QUADS); // LEFT PLANE
            glNormal3f(1,0,0); glVertex3f(-BOX_WIDTH/2, -BOX_HEIGHT/2, BOX_LENGTH / 2);
            glNormal3f(1,0,0); glVertex3f(-BOX_WIDTH/2, BOX_HEIGHT/2, BOX_LENGTH / 2);
            glNormal3f(1,0,0); glVertex3f(-BOX_WIDTH/2, BOX_HEIGHT/2, -BOX_LENGTH / 2);
            glNormal3f(1,0,0); glVertex3f(-BOX_WIDTH/2, -BOX_HEIGHT/2, -BOX_LENGTH / 2);
        glEnd();
        glBegin(GL_QUADS); // RIGHT PLANE
            glNormal3f(-1,0,0); glVertex3f(BOX_WIDTH/2, BOX_HEIGHT/2, BOX_LENGTH / 2);
            glNormal3f(-1,0,0); glVertex3f(BOX_WIDTH/2, -BOX_HEIGHT/2, BOX_LENGTH / 2);
            glNormal3f(-1,0,0); glVertex3f(BOX_WIDTH/2, -BOX_HEIGHT/2, -BOX_LENGTH / 2);
            glNormal3f(-1,0,0); glVertex3f(BOX_WIDTH/2, BOX_HEIGHT/2, -BOX_LENGTH / 2);
        glEnd();
        glBegin(GL_QUADS); // BACK
            glNormal3f(0,0,-1); glVertex3f(-BOX_WIDTH/2, BOX_HEIGHT/2, -BOX_LENGTH / 2);
            glNormal3f(0,0,-1); glVertex3f(BOX_WIDTH/2, BOX_HEIGHT/2, -BOX_LENGTH / 2);
            glNormal3f(0,0,-1); glVertex3f(BOX_WIDTH/2, -BOX_HEIGHT/2, -BOX_LENGTH / 2);
            glNormal3f(0,0,-1); glVertex3f(-BOX_WIDTH/2, -BOX_HEIGHT/2, -BOX_LENGTH / 2);
        glEnd();
        glBegin(GL_QUADS);
            glNormal3f(1,0,0); glVertex3f(0, BOX_HEIGHT, -2*BOX_LENGTH);
            glNormal3f(1,0,0); glVertex3f(0, 1.25*BOX_HEIGHT, -3.9*BOX_LENGTH);
            glNormal3f(1,0,0); glVertex3f(0, -1.25*BOX_HEIGHT, -3.9*BOX_LENGTH);
            glNormal3f(1,0,0); glVertex3f(0, -BOX_HEIGHT, -2*BOX_LENGTH);
        glEnd();
        glBegin(GL_QUADS);
            glNormal3f(-1,0,0); glVertex3f(0, 1.25*BOX_HEIGHT, -3.9*BOX_LENGTH);
            glNormal3f(-1,0,0); glVertex3f(0, BOX_HEIGHT, -2*BOX_LENGTH);
            glNormal3f(-1,0,0); glVertex3f(0, -BOX_HEIGHT, -2*BOX_LENGTH);
            glNormal3f(-1,0,0); glVertex3f(0, -1.25*BOX_HEIGHT, -3.9*BOX_LENGTH);
        glEnd();
    }
     if(lod == LOD_1)
    {
        lod_1_mesh->draw();
    }
    else
    {
        lod_2_mesh->draw();
    }
    wheelSection->render(speed, delta);
}