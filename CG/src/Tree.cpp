#include "headers/Tree.hpp"

bool Tree::HAS_TEXT = false;
GLuint Tree::textures[2]= {0};
const GLfloat Tree::treeAmbient[4] = {0.125, 0.2, 0.125, 1.0};
const GLfloat Tree::treeDiffuse[4] = {0.4, 0.55, 0.41, 1.0};
const GLfloat Tree::treeSpecular[4] = {0.15, 0.25, 0.15, 1.0};
const GLfloat Tree::treeShininess = 0.2f;
/* CONSTRUCTOR */
Tree::Tree()
{
    if(!HAS_TEXT)
    {
        textures[0] = TextureStore::getInstance()->getTexture("textures/tree/tree_0.png");
	textures[1] = TextureStore::getInstance()->getTexture("textures/tree/tree_1.png");
	HAS_TEXT = true;
    }
    selectedText = rand() % 2;
}
/* ALTERNATE CONSTRUCTOR */
Tree::Tree(float x, float y, float z, SceneObject* parent):SceneObject(x,y,z, parent)
{
    if(!HAS_TEXT)
    {
        textures[0] = TextureStore::getInstance()->getTexture("textures/tree/tree_0.png");
	textures[1] = TextureStore::getInstance()->getTexture("textures/tree/tree_1.png");
	HAS_TEXT = true;
    }
    selectedText = rand() % 2;
}
/*
    draw
    Draws the tree
    IMPORT:
        float - speed - Rate animations should run at // Not used currently could be for say swaying of trees
        float - delta - Time since the last frame
*/
void Tree::draw(float speed, float delta)
{  
    glMaterialfv(GL_FRONT, GL_AMBIENT, treeAmbient);
    glMaterialfv(GL_FRONT, GL_DIFFUSE, treeDiffuse);
    glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, treeSpecular);
    glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, treeShininess);  
    
    if(lod == LOD_0) // Just draw a quad
    {
        glColor3f(0.308, 0.672, 0.236); // Leaf green
        glBegin(GL_QUADS);
            glNormal3f(0, 0, 1); glVertex3f(-0.5f, 0, 0); // Bottom Left
            glNormal3f(0, 0, 1); glVertex3f(0.5f, 0, 0); // Bottom Right
            glNormal3f(0, 0, 1); glVertex3f(0.5f, 2.0f, 0);
            glNormal3f(0, 0, 1); glVertex3f(-0.5f, 2.0f, 0);
         glEnd();
         glColor3f(1, 1, 1);
    }
    if(lod == LOD_1)
    {
        glEnable (GL_ALPHA_TEST);
        glAlphaFunc(GL_GREATER, 0);
        
        glEnable(GL_TEXTURE_2D);
        glBindTexture(GL_TEXTURE_2D, textures[selectedText]);
        glBegin(GL_QUADS);
            glNormal3f(0, 0, 1);glTexCoord2f(0,0); glVertex3f(-0.5f, 0, 0); // Bottom Left
            glNormal3f(0, 0, 1);glTexCoord2f(1,0); glVertex3f(0.5f, 0, 0); // Bottom Right
            glNormal3f(0, 0, 1);glTexCoord2f(1,1); glVertex3f(0.5f, 2.0f, 0);
            glNormal3f(0, 0, 1);glTexCoord2f(0,1); glVertex3f(-0.5f, 2.0f, 0);
         glEnd();
         glDisable(GL_TEXTURE_2D);
         
         glDisable(GL_ALPHA);
    }
    else // use two quads to get a 3d effect
    {
        glEnable (GL_ALPHA_TEST);
        glAlphaFunc(GL_GREATER, 0);

        glEnable(GL_TEXTURE_2D);
        glBindTexture(GL_TEXTURE_2D, textures[selectedText]);
        
        glBegin(GL_QUADS);
            glNormal3f(0, 0, 1);glTexCoord2f(0,0); glVertex3f(-0.5f, 0, 0); // Bottom Left
            glNormal3f(0, 0, 1);glTexCoord2f(1,0); glVertex3f(0.5f, 0, 0); // Bottom Right
            glNormal3f(0, 0, 1);glTexCoord2f(1,1); glVertex3f(0.5f, 2.0f, 0);
            glNormal3f(0, 0, 1);glTexCoord2f(0,1); glVertex3f(-0.5f, 2.0f, 0);
         glEnd();
         
         glBegin(GL_QUADS); // Not entirely sure for what normals should be used
            glTexCoord2f(0,0); glVertex3f(0, 0, -0.5f); // Bottom Left
            glTexCoord2f(1,0); glVertex3f(0, 0, 0.5f); // Bottom Right
            glTexCoord2f(1,1); glVertex3f(0, 2.0f, 0.5);
            glTexCoord2f(0,1); glVertex3f(0, 2.0f, -0.5f);
         glEnd();
         
         glDisable(GL_TEXTURE_2D);
         glDisable(GL_ALPHA);
    }
    /* Reverse Material Bindings */
    glMaterialfv(GL_FRONT, GL_AMBIENT, defaultAmb);
    glMaterialfv(GL_FRONT, GL_DIFFUSE, defaultDiff);
    glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, defaultSpec);
    glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, defaultShine);  
}