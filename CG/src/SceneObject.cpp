#include "headers/SceneObject.hpp"

/* OPENGL DEFAULT MATERIALS SETTINGS */
const GLfloat SceneObject::defaultAmb[4] = {0.2f, 0.2f, 0.2f, 1.0f};
const GLfloat SceneObject::defaultDiff[4] = {0.8f, 0.8f, 0.8f, 1.0f};
const GLfloat SceneObject::defaultSpec[4] = {0, 0, 0, 1.0f};
const GLfloat SceneObject::defaultShine = 0;

/* CONSTRUCTOR */
SceneObject::SceneObject()
{ /* Setting base values here as to ensure that no undefined behaviour */
    position = new Vector3();
    scale = new Vector3(1.0f, 1.0f, 1.0f);
    rotation = new Vector3();
    parent = NULL;
    deltaAcc = 0;
    lod = NOT_DEF;
    inSight = true;
}
/* ALTERNATE CONSTRUCTOR */
SceneObject::SceneObject(float inx, float iny, float inz, SceneObject* inParent)
{
    position = new Vector3(inx, iny, inz);
    scale = new Vector3(1.0f, 1.0f, 1.0f);
    rotation = new Vector3();
    //parent = inParent;
    deltaAcc = 0;
    lod = NOT_DEF;
    parent = inParent;
    inSight = true;
}
/* DECONSTRUCTOR */
SceneObject::~SceneObject()
{
    delete position;
    delete scale;
    delete rotation;
}
/*
    movePosition
    Moves the position vector of the transform by select amounts
*/
void SceneObject::movePosition(float x, float y, float z)
{
    position->x += x;
    position->y += y;
    position->z += z;
}
/*
 * getPosition
 * Returns a pointer to the current position of the scene object
 */
Vector3* const SceneObject::getPosition()
{
    return position;
}
/*
    getGlobalPosition
    Calculates the position of the object in world space
*/
Vector3* SceneObject::getGlobalPosition()
{
    Vector3* result = new Vector3();
    Vector3* temp;
    if(parent != NULL) // If not a parent of any one then is already in world space
    {
        temp = parent->getGlobalPosition();
        *result += *temp;
        delete temp;
    }
    *result += *position;
    return result;
}
/*
    setPostion
    Moves the object to a given coordinate
*/
void SceneObject::setPosition(float x, float y, float z)
{
    position->x = x;
    position->y = y;
    position->z = z;
}
/*
 * setScale
 * Sets the objects scale
 */
void SceneObject::setScale(float x, float y, float z)
{
    scale->x = x;
    scale->y = y;
    scale->z = z;
}
void SceneObject::determineLOD()
{
    static const float DISTANCE_THIRD = CAMERA_SQR_FAR / 3;
    
    Vector3* worldPos = getGlobalPosition();
    const Vector3* camPos = Camera::getMainCamera()->getPosition();
    Vector3* distanceVec = *worldPos - *camPos;
    float sqrDistance = distanceVec->getSqrMagnitude(); // Trick from unity to save cycles from finding sqr root
    if(sqrDistance > DISTANCE_THIRD)
    {
        if(sqrDistance > DISTANCE_THIRD * 2) // Upper third
        {
            lod = LOD_0;
        }
        else // second third
        {
            lod = LOD_1;
        }
    }
    else
    {
        lod = LOD_2;
    }
    if(sqrDistance > CAMERA_SQR_FAR) // if greater than the clipping pane
    {
      lod = NONE;
    }
    delete distanceVec;
    delete worldPos;
}
/*
 * render
 * The template pattern that sets up the process for child classes draw methods
 */
void SceneObject::render(float speed, float delta)
{
    deltaAcc += delta;
    if(deltaAcc > (2 -(speed -1)) || lod == NOT_DEF) // every 2 seconds at 1 speed but change as speed changes
    {
        determineLOD();
        deltaAcc = 0;
    }
    if(lod != NONE)
    { 
      glPushAttrib(GL_ENABLE_BIT | GL_LIGHTING_BIT); // use "global" values
      glPushMatrix(); // Create a matrix
      glRotatef(rotation->x, 1, 0, 0);
      glRotatef(rotation->y, 0, 1, 0);
      glRotatef(rotation->z, 0, 0, 1);
      glTranslatef(position->x,position->y,position->z); // Set the postion
      glScalef(scale->x,scale->y,scale->z);
      draw(speed, delta);
      glPopMatrix(); // We are done here
      glPopAttrib();
    }
}
/*
 * rotateX
 * Sets the rotation to be used on the x axis
 */
void SceneObject::rotateX(float inx)
{
    rotation->x += inx;
}
/*
 * rotateY
 * Sets the rotation to be used on the y axis
 */
void SceneObject::rotateY(float iny)
{
    rotation->y += iny;
}
/*
 * rotateZ
 * Sets the rotation to be used on the z axis
 */
void SceneObject::rotateZ(float inz)
{
    rotation->z += inz;
}
/*
    isInSight
    Confirms if the object was calculated to be in sight
*/
bool SceneObject::isInSight()
{
    return inSight;
}
/*
    setIsInSight
    Sets the object to be in sight
*/
void SceneObject::setIsInSight(bool sightable)
{
    inSight = sightable;
}
