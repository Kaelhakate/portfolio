#ifndef HAS_GL
#define GL_GLEXT_PROTOTYPES
#include <GL/gl.h>
#include <GL/glut.h>
#include <GL/glx.h>
#include <GL/glext.h>

#define VAO_VERT 0
#define VAO_RGB 1
#define VAO_UV 2

#define HAS_GL
#endif

#include <iostream>
#include <exception>
#include <stdexcept>
#include <stdlib.h>
#include <time.h>
#include <stdio.h>
#include <GL/freeglut.h> // For glutSetOption

#include "headers/TextureStore.hpp"
#include "headers/MeshStore.hpp"
#include "headers/Scene.hpp"
#include "headers/Camera.hpp"

TextureStore* textStore;
MeshStore* meshStore;
Scene *scene;
Camera *mainCam;
GLenum shadeType = GL_SMOOTH;

const float CAM_ZOOM_IN = 0.1f;
const float CAM_ZOOM_OUT = -0.1f;

int window_width;
int window_height;

int animationSwitch = 0;
float animSpeed = 1.0f;
bool begunAnimation = false;

/*
 * drawText
 * Draws a array of chars on the string
 * IMPORT:
 *     char * - text - The array of chars
 */
void drawText(char* text)
{
    for(char* c = text; *c != '\0'; c++)
    {
        glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, *c);
    }
}
/*
 * drawString
 * Draws a string on to the screen.
 * IMPORT:
 *     std::string - text - The string to be desiplayed on the screen
 */
void drawString(const std::string text)
{
    for(const char* c = text.c_str(); *c != '\0'; c++)
    {
        glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, *c);
    }
}
/*
    drawUI
    draws the UI informattion to the screen
    Learnt from http://www.codersource.net/2011/01/27/displaying-text-opengl-tutorial-5/
    ^ The only non-shader solution I could find
    ^Learnt the switching process from https://www.youtube.com/watch?v=i1mp4zflkYo @3:20
    IMPORT:
      int - fps - The current frame rate to be displayed.    
*/
void drawUI(int fps)
{
    char fpsText[8];
    sprintf(fpsText, " %2d Fps", fps);
    
    glDisable(GL_LIGHTING);
    glDisable(GL_FOG);
    
    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();
    gluOrtho2D(0,0,window_width, window_height);
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glLoadIdentity();
    glColor3f(0,1,0);
    
    
    glRasterPos2f(-1, 0.95);
    drawText(fpsText);
    glRasterPos2f(-1, 0.85);
    if(shadeType == GL_SMOOTH)
    {
        drawString(" Smooth Shading");
    }
    else
    {
        drawString(" Flat Shading");
    }
    
    glRasterPos2f(-1, -0.875);
    drawString(" [Z/z] Zoom - [(X/x),(Y/y)] Rotation on said axis  - [p] - Flat shading - [P] - Smooth shading");
    glRasterPos2f(-1, -0.95);
    drawString(" [A/a] Begin Animation - [F/f] Increase animation speed - [S/s] Decrease Animation speed - [T/t] Pause Animation - [C/c] Resume animation");
    glColor3f(1,1,1); // Revert to white just to be sure
    glMatrixMode(GL_PROJECTION);
    glPopMatrix();
    glMatrixMode(GL_MODELVIEW);
    glPopMatrix();
    
    glEnable(GL_LIGHTING);
    glEnable(GL_FOG);
}
/*
    draw
    Creates the frame. First rendering the scene objects.
*/
void draw()
{
    glShadeModel(shadeType);
    ////// DELTA TIME CALCULATION https://stackoverflow.com/questions/22433014/c-opengl-how-do-i-get-delta-time
    static int deltaPreviousTime = 0;  
    int time = glutGet(GLUT_ELAPSED_TIME); // gets millisec time   
    float delta = (time - deltaPreviousTime) / 1000.0f; // change to per sec 
    deltaPreviousTime = time;
    /// FPS COUNTROL
    static int frameCount = 0;
    static int fpsPreviousTime = 0;
    static int fps;
    frameCount++;
    if(time - fpsPreviousTime > 1000)
    {
        fps = frameCount;
        fpsPreviousTime = time;
        frameCount = 0;
    }
    /// ACTUAL DRAWING PREP
    Camera* cam = Camera::getMainCamera();
    const Vector3* camFront = cam->getCamFront();
    const Vector3* camUp = cam->getCamUp();
    const Vector3* camPos = cam->getPosition();
    /* Clear Buffers */
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity(); // Start anew
    //orient camera
    gluLookAt(camPos->x, camPos->y, camPos->z, // Eye position
                    camPos->x + camFront->x, camPos->y + camFront->y, camPos->z+ camFront->z , // Target Postion
                    camUp->x, camUp->y, camUp->z); // Camera up
    scene->render(animationSwitch * animSpeed,delta);
    
    /// UI
    drawUI(fps);
    
    glFlush();
    glutSwapBuffers();
}
/*
    idle
    Called when the system is idling, redraws the previous frame to screen
*/
void idle(void)
{
    glutPostRedisplay();
}
/*
    keyBoardControl
    Handles when the user inputs buttons
*/
void keyBoardControl(unsigned char key, int x, int y)
{
    const float ROTATION_SPEED = 0.1f;
    static uint speedLevel = 9; // so we dont go below 0 as it gets weird for some of the animations
    float fov = Camera::getMainCamera()->getFOV();
    switch(key)
    {
        case 'A': // Using fall through case and int for animation resuming
        case 'a': animationSwitch = 1;
		  speedLevel = 9; // Reset switch for convienience
		  animSpeed = 1;
                  begunAnimation = true;
                  break;
        case 'T':
        case 't': animationSwitch = 0;
                  break;
        case 'C':
        case 'c': if(begunAnimation)
                  {
                     animationSwitch = 1;
                  }
                  break;                      
        case 'z': if(fov > 30)
		  {
		    glMatrixMode(GL_PROJECTION); // Playing around with the projection
		    glLoadIdentity();
		    Camera::getMainCamera()->setFOV(Camera::getMainCamera()->getFOV() - CAM_ZOOM_IN );
		    fov = Camera::getMainCamera()->getFOV();
		    gluPerspective(fov, Camera::getMainCamera()->getAspect(), Camera::zNear, Camera::zFar);
		    glMatrixMode(GL_MODELVIEW);// Go back to modelling mode
		  }
                  break;
        case 'Z': if(fov < 90)
		  {
		    glMatrixMode(GL_PROJECTION); // Playing around with the projection
		    glLoadIdentity();
		    Camera::getMainCamera()->setFOV(Camera::getMainCamera()->getFOV() - CAM_ZOOM_OUT );
		    fov = Camera::getMainCamera()->getFOV();
		    gluPerspective(fov, Camera::getMainCamera()->getAspect(), Camera::zNear, Camera::zFar);
		    glMatrixMode(GL_MODELVIEW);// Go back to modelling mode
		  }
                  break;
	case 'F':
	case 'f': animSpeed += 0.1f;
		  speedLevel++;
		  break;
	case 'S':
	case 's': if(speedLevel != 0)
		  {
		    animSpeed -= 0.1f;
		    speedLevel--;
		  }
		  break;
        case 'X': scene->rotateX(ROTATION_SPEED);
                  break;
        case 'x': scene->rotateX(-ROTATION_SPEED);
                  break;
        case 'Y': scene->rotateY(ROTATION_SPEED);
                  break;
        case 'y': scene->rotateY(-ROTATION_SPEED);
                  break;
        case 'p': shadeType = GL_FLAT;
                  break;
        case 'P': shadeType = GL_SMOOTH;
                  break;
        //default: // NOTHING
    }
}
/*
    reShape
    Called for window resizes.
    Based off https://www.ntu.edu.sg/home/ehchua/programming/opengl/CG_Examples.html
*/
void resizeWindow(GLsizei width, GLsizei height)
{
    Camera* cam = Camera::getMainCamera();
    
    if(height == 0)
    {
        height = 1; //Stop div 0
    }
    
    GLfloat aspect = (GLfloat)width/(GLfloat)height;
    glViewport(0,0, width, height);
    glMatrixMode(GL_PROJECTION); // Playing around with the projection
    glLoadIdentity();
    gluPerspective(cam->getFOV(), aspect, Camera::zNear, Camera::zFar);
    glMatrixMode(GL_MODELVIEW);// Go back to modelling mode
    cam->setAspect(aspect);
    window_width = width;
    window_height = height;
}
/*
    main
    The main loop of the program sets it all up
*/
int main(int argc, char** argv)
{
    int screen_width, screen_height;
    
        /* glut initiailization  needs to be called before glutGet*/
    glutInit(&argc, argv);
    
    srand(time(NULL));
    screen_width = glutGet(GLUT_SCREEN_WIDTH);
    screen_height = glutGet(GLUT_SCREEN_HEIGHT);
    window_width = screen_width * 0.75;
    window_height = screen_height * 0.75;
    
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize(window_width, window_height);
    
    /* Here we centre the screen */
    glutInitWindowPosition((screen_width - window_width)/2, (screen_height-window_height)/2);
    glutCreateWindow("CG Assignment - 18859407");
    try
    {
        GLuint vertArrayID;
        GLfloat nullAmbient[4] = {0,0,0,0};
        
        textStore = new TextureStore();
        meshStore = new MeshStore();
        mainCam = new Camera(0,1.5f,0);
        scene = new Scene(mainCam);
	scene->movePosition(1.25f, 0, 0); // We drive on the left side of the road

        glEnable(GL_NORMALIZE);
        glEnable(GL_LIGHTING);
        //glLightModelfv(GL_LIGHT_MODEL_AMBIENT, nullAmbient);
        glLightModelf(GL_LIGHT_MODEL_LOCAL_VIEWER, GL_TRUE); // use the eye coordinates as the specular viewpoint
        //glLightModeli(GL_LIGHT_MODEL_COLOR_CONTROL, GL_SEPARATE_SPECULAR_COLOR);
        glEnable(GL_LIGHT0); // The global sun
            GLfloat lpos[] = {0,-1,1,0.0f}; // Setting a directional light along the looking direction
            GLfloat lamb[] = {0.75, 0.75, 0.75, 1}; // Want reasonably bright ambient
            GLfloat ldiff[] = {1,1,1,1.0f};
            GLfloat lspec[] = {0.0f, 0.0f, 0.0f, 1.0f}; // We want no global specualar
            glLightfv(GL_LIGHT0, GL_POSITION, lpos);
            glLightfv(GL_LIGHT0, GL_AMBIENT, lamb);
            glLightfv(GL_LIGHT0, GL_DIFFUSE, ldiff);
            glLightfv(GL_LIGHT0, GL_SPECULAR, lspec);
            
        glEnable(GL_LIGHT1); // Specular for the road
            GLfloat sunPos[] = {0, 1, -10, 1.0f}; // Set the sun point light far in the distance to get the specualr reflection
            GLfloat sunAmbient[] = {0,0,0,1.0f}; // Want no ambient
            GLfloat sunDiffuse[] = {0.75f, 0.75f, 0.75f, 1.0f};
            GLfloat sunSpec[] = {1.0f, 1.0f, 1.0f, 1.0f}; // Want the brightest specular
            glLightfv(GL_LIGHT1, GL_POSITION, sunPos);
            glLightfv(GL_LIGHT1, GL_AMBIENT, sunAmbient);
            glLightfv(GL_LIGHT1, GL_DIFFUSE, sunDiffuse);
            glLightfv(GL_LIGHT1, GL_SPECULAR, sunSpec);
            
        glEnable(GL_LIGHT2); // Third light to provide specular for the wmills
            GLfloat sunPosTwo[] = {0, 6, 10, 1.0f}; // Set the sun point light far in the distance to get the specualr reflection
            GLfloat sunDiffuseTwo[] = {0.9f, 0.9f, 0.9f, 1.0f};
            glLightfv(GL_LIGHT2, GL_POSITION, sunPosTwo);
            glLightfv(GL_LIGHT2, GL_AMBIENT, sunAmbient);
            glLightfv(GL_LIGHT2, GL_DIFFUSE, sunDiffuseTwo);
            glLightfv(GL_LIGHT2, GL_SPECULAR, sunSpec);
        // FOG
            GLfloat fColor[4] = {0.65f,0.92f,0.93f,0.5};
            glFogi(GL_FOG_MODE, GL_LINEAR);
            glFogf(GL_FOG_START, 5.0f);
            glFogf(GL_FOG_END, 150);
            glFogf(GL_FOG_DENSITY, 0.35f);
            glFogfv(GL_FOG_COLOR, fColor);
            glEnable(GL_FOG);
        glEnable(GL_DEPTH_TEST);
        glEnable(GL_CULL_FACE);
        glCullFace(GL_BACK);
        glDisable(GL_COLOR_MATERIAL);
        /* CALLBACKS */
        glutDisplayFunc(draw);
        glutReshapeFunc(resizeWindow);
        glutIdleFunc(idle);
        glutKeyboardFunc(keyBoardControl);
        //glutSpecialFunc(specialKeyBoardControl);
        /* 
            Need the following to avoid the memory leaks I was getting
            Thanks to https://stackoverflow.com/questions/2688098/cleaning-up-when-exiting-an-opengl-app 
        */
        glutSetOption(GLUT_ACTION_ON_WINDOW_CLOSE, GLUT_ACTION_CONTINUE_EXECUTION);
        
        glutMainLoop();
    }
    catch(std::exception& ex) // Lazy polymorphism
    {
        std::cerr << "ERROR: " << ex.what() <<"\n";
    }
    
    if(scene != NULL)
    {
        delete scene;
    }
    if(mainCam != NULL)
    {
        delete mainCam;
    }
    if(meshStore != NULL)
    {
        delete meshStore;
    }
    if(textStore != NULL)
    {
        delete textStore;
    }
    return 0;
}
