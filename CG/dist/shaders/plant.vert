#version 120
/* Learnt from lecture slides Empirical formula and few tricks from here http://www.lighthouse3d.com/tutorials/glsl-12-tutorial/directional-lights-i/ */

void main()
{
    vec4 I, ambient, diffuse, specular;
    vec3 N,L;
    float NL;
    
    
    ambient = gl_FrontMaterial.ambient * gl_LightSource[0].ambient; // Using the suns ambient
    
    for(int ii=0; i < 2; ii++) // Only ever 2 lights
    {
        N = normalize(gl_NormalMatrix * gl_Normal); // Get the right normal (I believe after all the matrix operations)
        L = normalize(gl_LightSource[ii].position);
        NL = max(dot(N,L), 0.0); // Clamp the normal 
        R = ((2 * N) * NL) - L;
    }
   
    
    
    
    vec4 diffuse = gl_FrontMaterial.diffuse * 
    

    vec3 normal, lightDir;
    vec4 diffuse;

    normal = normalize(gl_NormalMatrix * gl_Normal);
    lightDir = normalize(vec3(gl_LightSource[0].position));
    float dotProd = max(dot(normal, lightDir), 0.0);
    
    diffuse = gl_FrontMaterial.diffuse * gl_LightSource[0].diffuse;
    
    
    
    
    
    
    
    
    
    
    
    
    gl_Position =  gl_ModelViewProjectionMatrix * gl_Vertex;
    /* Passing along values to fragement shader */
    gl_FrontColor = dotProd * diffuse;
    gl_TexCoord[0] = gl_TextureMatrix[0] * gl_MultiTexCoord0;
}