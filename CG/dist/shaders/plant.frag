#version 120
/* My first Shader learnt from here: http://www.morethantechnical.com/2013/11/09/vertex-array-objects-with-shaders-on-opengl-2-1-glsl-1-2-wcode/ */


uniform sampler2D textureRGBA;

varying vec2 textCoord;

void main()
{
    vec4 textcol = texture2D(textureRGBA, gl_TexCoord[0].xy);
    
    gl_FragColor = textcol;
}