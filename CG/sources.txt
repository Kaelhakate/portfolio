SkyBox
http://www.humus.name/index.php?page=Textures&ID=81

Terrain - grass.jpg
https://www.textures.com/download/grass0165/50559
Terrain - road.jpg
https://www.textures.com/download/roads0059/35209
Tree - tree_0.png
https://www.textures.com/download/branches0039/111263
Tree - tree_1.png
https://www.textures.com/download/branches0038/111262

Plants - 0
https://www.textures.com/download/natureplants0026/107853
Plants - 1
https://www.textures.com/download/natureplants0019/107846
Plants - 2
https://www.textures.com/download/natureplants0056/111271
Plants - 3
https://www.textures.com/download/natureplants0060/111275
Plants - 4
https://www.textures.com/download/natureplants0043/111258
Plants - 5
https://www.textures.com/download/natureplants0038/111253
Plants - 6
https://www.textures.com/download/natureplants0073/111288
Plants - 7
https://www.textures.com/download/natureplants0068/111283
Plants - 8
https://www.textures.com/download/natureplants0006/107831


Windmill Model
https://free3d.com/3d-model/mill-58987.html
WindMill Texture
https://www.textures.com/download/metalrusted0081/37337
Shed Texture
https://www.textures.com/download/metalplatesrusted0052/7832
Wood Texture(Power Pylons)
https://www.textures.com/download/woodrough0050/4774